﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class AddedCWVScanInfoTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CWVInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SiteLinkItemId = table.Column<int>(type: "int", nullable: false),
                    UrlUsed = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsMetricScanCompleted = table.Column<bool>(type: "bit", nullable: false),
                    Performance = table.Column<int>(type: "int", nullable: false),
                    Accessibility = table.Column<int>(type: "int", nullable: false),
                    BestPractices = table.Column<int>(type: "int", nullable: false),
                    SEO = table.Column<int>(type: "int", nullable: false),
                    FirstContentfulPaint = table.Column<float>(type: "real", nullable: false),
                    TimeToInteractive = table.Column<float>(type: "real", nullable: false),
                    SpeedIndex = table.Column<float>(type: "real", nullable: false),
                    TotalBlockingTime = table.Column<float>(type: "real", nullable: false),
                    LargestContentfulPain = table.Column<float>(type: "real", nullable: false),
                    CumulativeLayoutShift = table.Column<float>(type: "real", nullable: false),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastModified = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CWVInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CWVInfo_SiteLinkItems_SiteLinkItemId",
                        column: x => x.SiteLinkItemId,
                        principalTable: "SiteLinkItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CWVInfo_SiteLinkItemId",
                table: "CWVInfo",
                column: "SiteLinkItemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CWVInfo");
        }
    }
}
