﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class AddedRefCode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ReferenceClientCode",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SiteLinkItemId = table.Column<int>(type: "int", nullable: true),
                    ClientCodeId = table.Column<int>(type: "int", nullable: false),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastModified = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReferenceClientCode", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReferenceClientCode_ClientCode_ClientCodeId",
                        column: x => x.ClientCodeId,
                        principalTable: "ClientCode",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ReferenceClientCode_SiteLinkItems_SiteLinkItemId",
                        column: x => x.SiteLinkItemId,
                        principalTable: "SiteLinkItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ReferenceClientCode_ClientCodeId",
                table: "ReferenceClientCode",
                column: "ClientCodeId");

            migrationBuilder.CreateIndex(
                name: "IX_ReferenceClientCode_SiteLinkItemId",
                table: "ReferenceClientCode",
                column: "SiteLinkItemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ReferenceClientCode");
        }
    }
}
