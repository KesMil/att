﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class SiteLinkItemUpdated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SiteItemId",
                table: "SiteLinkItems",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_SiteLinkItems_SiteItemId",
                table: "SiteLinkItems",
                column: "SiteItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_SiteLinkItems_SiteItems_SiteItemId",
                table: "SiteLinkItems",
                column: "SiteItemId",
                principalTable: "SiteItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SiteLinkItems_SiteItems_SiteItemId",
                table: "SiteLinkItems");

            migrationBuilder.DropIndex(
                name: "IX_SiteLinkItems_SiteItemId",
                table: "SiteLinkItems");

            migrationBuilder.DropColumn(
                name: "SiteItemId",
                table: "SiteLinkItems");
        }
    }
}
