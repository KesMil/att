﻿Adding new migration:
1. Open Package Manager Console
2. Set "Default Project" dropdown to "Infrastructure".
3. Execute > Add-Migration MIGRATIONS_NAME -o "Persistence/Migrations"
4. Execute > Update-Database