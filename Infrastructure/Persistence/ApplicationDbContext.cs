﻿using Application.Interfaces;
using Domain.Common;
using Domain.Entities;
using Infrastructure.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Persistence
{
    // This is the class that maps to the database tables. It is used by Entity Framework for database access.
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        private readonly ICurrentUserService _currentUserService;

        public ApplicationDbContext(DbContextOptions options, ICurrentUserService currentUserService) : base(options)
        {
            // test if working auto migrations here
            this.Database.Migrate();
            this._currentUserService = currentUserService;
        }

        public DbSet<TestItem> TestItems { get; set; }
        public DbSet<SiteItem> SiteItems { get; set; }
        public DbSet<SiteLinkItem> SiteLinkItems { get; set; }
        public DbSet<ClientCode> ClientCode { get; set; }
        public DbSet<ReferenceClientCode> ReferenceClientCode { get; set; }
        public DbSet<DiffIgnoreRegex> DiffIgnoreRegexes { get; set; }
        public DbSet<CWVInfo> CWVInfo { get; set; }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            // Implement change tracking logic here
            foreach (Microsoft.EntityFrameworkCore.ChangeTracking.EntityEntry<AuditableEntity> entry in ChangeTracker.Entries<AuditableEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedBy = _currentUserService.UserId;
                        entry.Entity.Created = DateTime.Now;
                        break;

                    case EntityState.Modified:
                        entry.Entity.LastModifiedBy = _currentUserService.UserId;
                        entry.Entity.LastModified = DateTime.Now;
                        break;
                }
            }

            var result = await base.SaveChangesAsync(cancellationToken);
            return result;
        }
    }
}
