﻿using Application.Common.BackgroundTasks;
using Application.Common.BackgroundTasks.Interfaces;
using Application.Interfaces;
using Hangfire;
using Hangfire.Mongo;
using Hangfire.Mongo.Migration.Strategies;
using Hangfire.Mongo.Migration.Strategies.Backup;
using Application.Common.BackgroundTasks.Executors;
using System;
using System.Collections.Generic;
using System.Text;
using Application.Common.BackgroundTasks.Tasks;
using Microsoft.Extensions.Configuration;

namespace Infrastructure
{
    // Class for enqueuing new tasks. ITaskManager abstracts the usage of Hangfire library directly by the project, which enables us to easily replace Hangfire with something else.
    public class TasksManager : ITasksManager
    {
        LogHandler _logHandler;
        public TasksManager(IConfiguration configuration)
        {
            GlobalConfiguration.Configuration.UseMongoStorage(configuration.GetConnectionString("HangfireConnection"), new MongoStorageOptions()
            {
                MigrationOptions = new MongoMigrationOptions()
                {
                    MigrationStrategy = new MigrateMongoMigrationStrategy(),
                    BackupStrategy = new CollectionMongoBackupStrategy()
                },
                InvisibilityTimeout = TimeSpan.FromMinutes(5),
                QueuePollInterval = TimeSpan.FromSeconds(1)
            });

            _logHandler = new LogHandler(); // TODO: Why is this here? Forgot what i was about to do with it. Consider removing.
        }
        public void EnqueueTask<T>(T taskData) where T : IBackgroundTask
        {
            BackgroundJob.Enqueue<ITaskHandler<T>>(x => x.Execute(taskData));
        }
    }
}