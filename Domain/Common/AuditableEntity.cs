﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Common
{
    // This class is a parent class for all other database entities. It enables all entities to be tracked automatically by adding timestamps and user identity when data is creatd or modified.
    public abstract class AuditableEntity
    {
        public DateTime Created { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? LastModified { get; set; }

        public string LastModifiedBy { get; set; }
    }
}
