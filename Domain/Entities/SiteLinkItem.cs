﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class SiteLinkItem : AuditableEntity
    {
        public int Id { get; set; }
        public int SiteItemId { get; set; }
        public string Url { get; set; }
        public virtual SiteItem SiteItem { get; set; }
    }
}