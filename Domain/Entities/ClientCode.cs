﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class ClientCode : AuditableEntity
    {
        public int Id { get; set; }
        public int SiteLinkItemId { get; set; }
        public string UrlUsed { get; set; }
        public string Code { get; set; }
        public bool IsDownloadingCompleted { get; set; }
        public virtual SiteLinkItem SiteLinkItem { get; set; }
        public int? ResponseCode { get; set; }
    }
}
