﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class SiteItem : AuditableEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Host { get; set; }
    }
}
