﻿using Domain.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class CWVInfo : AuditableEntity
    {
        //Common properties
        public int Id { get; set; }
        public int SiteLinkItemId { get; set; }
        public string UrlUsed { get; set; }
        public bool IsMetricScanCompleted { get; set; }
        public virtual SiteLinkItem SiteLinkItem { get; set; }

        //Key metrics
        public int Performance { get; set; }
        public int Accessibility { get; set; }
        public int BestPractices { get; set; }
        public int SEO { get; set; }

        //Additional Metrics
        [Column(TypeName = "decimal(6, 2)")]
        public decimal FirstContentfulPaint { get; set; }
        [Column(TypeName = "decimal(6, 2)")]
        public decimal TimeToInteractive { get; set; }
        [Column(TypeName = "decimal(6, 2)")]
        public decimal SpeedIndex { get; set; }
        [Column(TypeName = "decimal(6, 2)")]
        public decimal TotalBlockingTime { get; set; }
        [Column(TypeName = "decimal(6, 2)")]
        public decimal LargestContentfulPain { get; set; }
        [Column(TypeName = "decimal(6, 2)")]
        public decimal CumulativeLayoutShift { get; set; }

        // Scan info
        public bool IsScanSuccessful { get; set; }
        public string Source { get; set; }
    }
}
