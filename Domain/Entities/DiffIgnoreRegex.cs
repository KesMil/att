﻿using Domain.Common;

namespace Domain.Entities
{
    public class DiffIgnoreRegex : AuditableEntity
    {
        public int Id { get; set; }
        public int SiteItemId { get; set; }
        public string RegexLines { get; set; }
        public virtual SiteItem SiteItem { get; set; }
    }
}