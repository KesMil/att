﻿using Domain.Common;

namespace Domain.Entities
{
    public class ReferenceClientCode : AuditableEntity
    {
        public int Id { get; set; }
        public int? SiteLinkItemId { get; set; } // NULLABLE to avoid cyclic dependency. ClientCode is also referencing SiteLinkItem, this causes this issue.
        public int ClientCodeId { get; set; }
        public virtual SiteLinkItem SiteLinkItem { get; set; }
        public virtual ClientCode ClientCode { get; set; }
    }
}
