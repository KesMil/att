using Hangfire;
using Hangfire.Mongo;
using Hangfire.Mongo.Migration.Strategies;
using Hangfire.Mongo.Migration.Strategies.Backup;
using Hangfire.SqlServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace WorkerService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly JobActivator _jobActivator;
        private readonly IConfiguration _configuration;

        public Worker(ILogger<Worker> logger, JobActivator jobActivator, IConfiguration configuration)
        {
            _logger = logger;
            _jobActivator = jobActivator;
            _configuration = configuration;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            GlobalConfiguration.Configuration.UseMongoStorage(_configuration.GetConnectionString("HangfireConnection"), new MongoStorageOptions()
            {
                // Configure database for worker service.
                MigrationOptions = new MongoMigrationOptions()
                {
                    MigrationStrategy = new MigrateMongoMigrationStrategy(),
                    BackupStrategy = new CollectionMongoBackupStrategy()
                },
                InvisibilityTimeout = TimeSpan.FromMinutes(5),
                QueuePollInterval = TimeSpan.FromSeconds(1)
            });

            // Job Activator provides the mechanism to return object that has implementation for tasks execution. Each task has it's own executor object, which is returned by this _jobActivator object.
            GlobalConfiguration.Configuration.UseActivator<JobActivator>(_jobActivator);

            _logger.LogInformation("Starting worker service at: {time}", DateTimeOffset.Now);

            var options = new BackgroundJobServerOptions
            {
                // Configure worker service to scale based on cpu core cout.
                WorkerCount = (Environment.ProcessorCount > 1) ? (Environment.ProcessorCount / 2) : 1
            };

            var server = new BackgroundJobServer(options);

            _logger.LogInformation("Worker service started at: {time}", DateTimeOffset.Now);

            while (!stoppingToken.IsCancellationRequested)
            {
                await Task.Delay(1000, stoppingToken);
            }

            server.Dispose();
        }
    }
}
