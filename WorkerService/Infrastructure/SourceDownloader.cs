﻿using Application.Common.BackgroundTasks.Interfaces;
using Application.Common.BackgroundTasks.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WorkerService.Infrastructure
{
    // Service Implementation for retrieving client source code from URL provided.
    public class SourceDownloader : ISourceDownloader
    {
        private readonly HttpClient httpClient;

        public SourceDownloader(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        async Task<SourceDownloadedResponse> ISourceDownloader.GetSourceAsync(string url)
        {
            SourceDownloadedResponse result = new SourceDownloadedResponse();

            HttpResponseMessage response = await this.httpClient.GetAsync(url);

            result.SourceCode = await response.Content?.ReadAsStringAsync();
            result.ResponseCode = (int)response.StatusCode;

            return result;
        }
    }
}
