﻿using Application.Common.BackgroundTasks.Interfaces;
using Application.Common.BackgroundTasks.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WorkerService.Infrastructure
{
    // This class implements the logic to run lighthouse against url provided.
    public class CWVService : ICWVService
    {
        public CWVService() { }
        public Task<CWVScanResponse> GetCoreWebVitalsHtmlOutput(string url)
        {
            CWVScanResponse cwvScanResponse = new CWVScanResponse();
            cwvScanResponse.Started = DateTime.Now;

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                RunCWV(cwvScanResponse, url, "/bin/bash");
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                RunCWV(cwvScanResponse, url, "cmd.exe");
            }
            else
            {
                cwvScanResponse.Successful = false;
            }

            if (!string.IsNullOrEmpty(cwvScanResponse.SourceHTML) && !string.IsNullOrEmpty(cwvScanResponse.SourceJSON))
            {
                cwvScanResponse.Successful = true;
            }

            return Task.FromResult(cwvScanResponse);
        }

        private void RunCWV(CWVScanResponse cwvScanResponse, string url, string shellFileName)
        {
            string fileId = url.GetHashCode().ToString();

            var proc = new System.Diagnostics.Process();
            proc.StartInfo.CreateNoWindow = true;
            proc.StartInfo.RedirectStandardInput = true;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.UseShellExecute = false;
            // Bug? Has to be dissabled in order for lighthouse to function.
            //proc.StartInfo.RedirectStandardError = true;
            proc.StartInfo.FileName = shellFileName;

            proc.Start();

            proc.BeginOutputReadLine();

            proc.StandardInput.WriteLine($"lighthouse {url} --chrome-flags=\"--headless\" --output json,html --output-path=\"{fileId}\" --form-factor mobile --max-wait-for-load 30000");
            proc.StandardInput.WriteLine("exit");
            proc.WaitForExit();

            // Read files
            try
            {
                cwvScanResponse.SourceHTML = System.IO.File.ReadAllText(@$"{fileId}.report.html");
                cwvScanResponse.SourceJSON = System.IO.File.ReadAllText(@$"{fileId}.report.json");

                // Remove files
                System.IO.File.Delete(@$"{fileId}.report.html");
                System.IO.File.Delete(@$"{fileId}.report.json");
            }
            catch (Exception)
            {
                Console.WriteLine("Reading/Deleting files has failed.");
            }

            // Sanitize result
            //result = result.Replace("┬á", " ");
            //result = result.Replace("ΓÇö", "-");

            //return result.Trim();
        }
    }
}
