﻿using Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkerService.Infrastructure
{
    class CurrentUserService : ICurrentUserService
    {
        // We return worker hardcoded string to identify user as a worker service. This will be stored in database when results are generated.
        public string UserId => "WORKER_SERVICE";
    }
}
