﻿using Application.Common.BackgroundTasks;
using Hangfire;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkerService.Infrastructure
{
    // This class provides a mechanism to retrieve the object that implements the logic for tasks execution.
    // Each task has it's own executing object, and this is the service to retrieve the correct executor object.
    class ContainerJobActivator : JobActivator
    {
        private readonly IServiceProvider _serviceProvider;

        public ContainerJobActivator(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        public override object ActivateJob(Type jobType)
        {
            var scope = _serviceProvider.CreateScope();

            var jobHandler =
                scope.ServiceProvider
                    .GetRequiredService(jobType);

            return jobHandler;
        }
    }
}
