using Application.Common.BackgroundTasks;
using Application.Common.BackgroundTasks.Executors;
using Application.Common.BackgroundTasks.Interfaces;
using Application.Common.BackgroundTasks.Tasks;
using Application.Interfaces;
using FluentAssertions.Common;
using Hangfire;
using Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkerService.Infrastructure;

namespace WorkerService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args).ConfigureAppConfiguration((hostContext, configurationBuilder) =>
            {
                // Use development configuration when developing.
                if (hostContext.HostingEnvironment.IsDevelopment())
                {
                    // It is also possible to use multiple files for development just by changing the variable with file name.
                    const string environment = "Development";

                    configurationBuilder
                    .AddJsonFile("appsettings.json")
                    .AddJsonFile($"appsettings.{environment}.json", optional: true);
                }
            })
                .ConfigureServices((hostContext, services) =>
                {
                    // Add all required services to DI container, also configure where required.
                    services.AddHostedService<Worker>();
                    services.AddSingleton<JobActivator, ContainerJobActivator>();

                    services.AddScoped<ICurrentUserService, CurrentUserService>();
                    services.AddDbContext<ApplicationDbContext>(options =>
                    options.UseSqlServer(
                        hostContext.Configuration.GetConnectionString("DefaultConnection"),
                        b => b.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)));

                    services.AddScoped<IApplicationDbContext, ApplicationDbContext>();

                    services.AddHttpClient<ISourceDownloader, SourceDownloader>();
                    services.AddScoped<ICWVService, CWVService>();
                    services.AddScoped<ITaskHandler<LogTask>, LogHandler>();
                    services.AddScoped<ITaskHandler<GetClientCodeTask>, GetClientCodeHandler>();
                    services.AddScoped<ITaskHandler<ScanCoreWebVitalsTask>, ScanCoreWebVitalsHandler>();
                });
    }
}