﻿using Application.Common.Mappings;
using AutoMapper;
using Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.DTOs
{
    public class AuditableEntityDto : IMapFrom<AuditableEntity>
    {
        public DateTime Created { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? LastModified { get; set; }

        public string LastModifiedBy { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<AuditableEntity, AuditableEntityDto>();
        }
    }
}
