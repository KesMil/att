﻿using Application.Common.Mappings;
using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.DTOs
{
    public class CWVInfoDto : AuditableEntityDto, IMapFrom<CWVInfo>
    {
        public int Id { get; set; }
        public int SiteLinkItemId { get; set; }
        public string UrlUsed { get; set; }
        public bool IsMetricScanCompleted { get; set; }

        //Key metrics
        public int Performance { get; set; }
        public int Accessibility { get; set; }
        public int BestPractices { get; set; }
        public int SEO { get; set; }

        //Additional Metrics
        public decimal FirstContentfulPaint { get; set; }
        public decimal TimeToInteractive { get; set; }
        public decimal SpeedIndex { get; set; }
        public decimal TotalBlockingTime { get; set; }
        public decimal LargestContentfulPain { get; set; }
        public decimal CumulativeLayoutShift { get; set; }

        // Scan info props
        public bool IsScanSuccessful { get; set; }
        public string Source { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<CWVInfo, CWVInfoDto>();
        }
    }
}
