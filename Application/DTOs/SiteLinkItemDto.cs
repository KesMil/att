﻿using Application.Common.Mappings;
using AutoMapper;
using Domain.Entities;

namespace Application.DTOs
{
    public class SiteLinkItemDto : AuditableEntityDto, IMapFrom<SiteLinkItem>
    {
        public int Id { get; set; }
        public int SiteItemId { get; set; }
        public string Url { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<SiteLinkItem, SiteLinkItemDto>();
        }
    }
}
