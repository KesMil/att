﻿using Application.Common.Mappings;
using AutoMapper;
using Domain.Entities;

namespace Application.DTOs
{
    public class ClientCodeDto : AuditableEntityDto, IMapFrom<ClientCode>
    {
        public int Id { get; set; }
        public int SiteLinkItemId { get; set; }
        public string UrlUsed { get; set; }
        public string Code { get; set; }
        public bool IsDownloadingCompleted { get; set; }
        public int? ResponseCode { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<ClientCode, ClientCodeDto>();
        }
    }
}
