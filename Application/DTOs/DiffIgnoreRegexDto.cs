﻿using Application.Common.Mappings;
using AutoMapper;
using Domain.Entities;

namespace Application.DTOs
{
    public class DiffIgnoreRegexDto : AuditableEntityDto, IMapFrom<DiffIgnoreRegex>
    {
        public int Id { get; set; }
        public int SiteItemId { get; set; }
        public string RegexLines { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<DiffIgnoreRegex, DiffIgnoreRegexDto>();
        }
    }
}
