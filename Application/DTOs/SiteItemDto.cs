﻿using Application.Common.Mappings;
using AutoMapper;
using Domain.Entities;

namespace Application.DTOs
{
    public class SiteItemDto : AuditableEntityDto, IMapFrom<SiteItem>
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Host { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<SiteItem, SiteItemDto>();
        }
    }
}