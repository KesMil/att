﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.BackgroundTasks.Types
{
    public class SourceDownloadedResponse
    {
        public string SourceCode { get; set; }
        public int ResponseCode { get; set; }
    }
}
