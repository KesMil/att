﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.BackgroundTasks.Types
{
    public class CWVScanResponse
    {
        public DateTime Started { get; set; }
        public DateTime Completed { get; set; }
        public bool Successful { get; set; }
        public string SourceHTML { get; set; }
        public string SourceJSON { get; set; }
    }
}
