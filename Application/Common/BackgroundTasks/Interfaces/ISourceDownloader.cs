﻿using Application.Common.BackgroundTasks.Types;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Common.BackgroundTasks.Interfaces
{
    public interface ISourceDownloader
    {
        Task<SourceDownloadedResponse> GetSourceAsync(string url);
    }
}