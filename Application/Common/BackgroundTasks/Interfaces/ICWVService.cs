﻿using Application.Common.BackgroundTasks.Types;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Common.BackgroundTasks.Interfaces
{
    public interface ICWVService
    {
        public Task<CWVScanResponse> GetCoreWebVitalsHtmlOutput(string url);
    }
}
