﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.BackgroundTasks.Interfaces
{
    public interface ITaskHandler<T> where T : IBackgroundTask
    {
        void Execute(T taskData);
    }
}
