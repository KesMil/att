﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.BackgroundTasks.Interfaces
{
    public interface IBackgroundTask
    {
        string Id { get; set; }
    }
}
