﻿using Application.Common.BackgroundTasks.Interfaces;
using Application.Common.BackgroundTasks.Tasks;
using Application.Common.BackgroundTasks.Types;
using Application.Interfaces;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Threading;

namespace Application.Common.BackgroundTasks.Executors
{
    public class ScanCoreWebVitalsHandler : ITaskHandler<ScanCoreWebVitalsTask>
    {
        private readonly IApplicationDbContext context;
        private readonly ICWVService cwvService;

        public ScanCoreWebVitalsHandler(IApplicationDbContext context, ICWVService cwvService)
        {
            this.context = context;
            this.cwvService = cwvService;
        }
        public async void Execute(ScanCoreWebVitalsTask taskData)
        {
            Console.WriteLine($"Link url to scan CWV: {taskData.Url}");

            Domain.Entities.CWVInfo cwvInfo = this.context.CWVInfo.Where(cwv => cwv.Id == taskData.CWVInfoId).FirstOrDefault();
            if (cwvInfo == null)
            {
                Console.WriteLine("CoreWebVitals database record not found while starting task.");
                return;
            }

            CWVScanResponse cwvResponse = new CWVScanResponse();

            if (Uri.IsWellFormedUriString(taskData.Url, UriKind.Absolute))
            {
                cwvResponse = await cwvService.GetCoreWebVitalsHtmlOutput(taskData.Url);
            }

            Console.WriteLine($"Scan completed for: {taskData.Url} | Successful: {cwvResponse.Successful}");

            cwvInfo = this.context.CWVInfo.Where(cwv => cwv.Id == taskData.CWVInfoId).FirstOrDefault();

            if (cwvInfo != null)
            {
                cwvInfo.IsMetricScanCompleted = true;
                cwvInfo.IsScanSuccessful = cwvResponse.Successful;

                if (cwvResponse.Successful)
                {
                    cwvInfo.Source = cwvResponse.SourceHTML;
                    try
                    {
                        JObject jt = JObject.Parse(cwvResponse.SourceJSON);
                        cwvInfo.Performance = (int)Math.Truncate(float.Parse(jt["categories"]["performance"]["score"].ToString()) * 100);
                        cwvInfo.Accessibility = (int)Math.Truncate(float.Parse(jt["categories"]["accessibility"]["score"].ToString()) * 100);
                        cwvInfo.BestPractices = (int)Math.Truncate(float.Parse(jt["categories"]["best-practices"]["score"].ToString()) * 100);
                        cwvInfo.SEO = (int)Math.Truncate(float.Parse(jt["categories"]["seo"]["score"].ToString()) * 100);
                    }
                    catch (Exception)
                    {
                        cwvInfo.IsScanSuccessful = false;
                    }
                }

                await this.context.SaveChangesAsync(new CancellationToken());
                Console.WriteLine($"Completed the scan of CWV for {taskData.Url}");
            }
            else
            {
                Console.WriteLine("CoreWebVitals database record not found after task has been finised."); // TODO: integrate logger solution
            }
        }
    }
}