﻿using Application.Common.BackgroundTasks.Interfaces;
using Application.Common.BackgroundTasks.Tasks;
using Application.Common.BackgroundTasks.Types;
using Application.Interfaces;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Linq;
using System.Threading;

namespace Application.Common.BackgroundTasks.Executors
{
    public class GetClientCodeHandler : ITaskHandler<GetClientCodeTask>
    {
        private readonly IApplicationDbContext context;
        private readonly ISourceDownloader sourceDownloader;

        public GetClientCodeHandler(IApplicationDbContext context, ISourceDownloader sourceDownloader)
        {
            this.context = context;
            this.sourceDownloader = sourceDownloader;
        }
        public async void Execute(GetClientCodeTask taskData)
        {
            SourceDownloadedResponse resp = null;

            if (Uri.IsWellFormedUriString(taskData.Url, UriKind.Absolute))
            {
                resp = await this.sourceDownloader.GetSourceAsync(taskData.Url);
            }

            Console.WriteLine($"Link url: {taskData.Url}");

            var clientCode = this.context.ClientCode.Where(cc => cc.Id == taskData.ClientCodeId).FirstOrDefault();
            if (clientCode != null)
            {
                clientCode.Code = resp?.SourceCode ?? String.Empty;
                clientCode.IsDownloadingCompleted = true;
                clientCode.ResponseCode = resp?.ResponseCode ?? 0;

                await this.context.SaveChangesAsync(new CancellationToken());
                Console.WriteLine("Completed");
            } else
            {
                Console.WriteLine("Client Code container not found."); // TODO: integrate logger solution
            }
        }
    }
}