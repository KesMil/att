﻿using Application.Common.BackgroundTasks.Interfaces;
using Application.Common.BackgroundTasks.Tasks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Application.Common.BackgroundTasks.Executors
{
    public class LogHandler : ITaskHandler<LogTask>
    {

        public void Execute(LogTask taskData)
        {
            Console.WriteLine($"Message data: {taskData.Message}");
            Thread.Sleep(5000);
            Console.WriteLine("Completed");
        }
    }
}