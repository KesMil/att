﻿using Application.Common.BackgroundTasks.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.BackgroundTasks.Abstractions
{
    public class BackgroundTaskBase : IBackgroundTask
    {
        public string Id { get; set; }
        public BackgroundTaskBase()
        {
            this.Id = Guid.NewGuid().ToString();
        }
    }
}
