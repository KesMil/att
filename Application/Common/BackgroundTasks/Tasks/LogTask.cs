﻿using Application.Common.BackgroundTasks.Abstractions;
using Application.Common.BackgroundTasks.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.BackgroundTasks.Tasks
{
    public class LogTask : BackgroundTaskBase
    {
        public string Message { get; set; }
    }
}
