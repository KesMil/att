﻿using Application.Common.BackgroundTasks.Abstractions;

namespace Application.Common.BackgroundTasks.Tasks
{
    public class ScanCoreWebVitalsTask : BackgroundTaskBase
    {
        public string Url { get; set; }
        public int CWVInfoId { get; set; }
    }
}