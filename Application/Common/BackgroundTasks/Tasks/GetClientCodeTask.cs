﻿using Application.Common.BackgroundTasks.Abstractions;
using Application.Common.BackgroundTasks.Enums;
using Application.Common.BackgroundTasks.Types;

namespace Application.Common.BackgroundTasks.Tasks
{
    public class GetClientCodeTask : BackgroundTaskBase
    {
        public string Url { get; set; }
        public int ClientCodeId { get; set; }
    }
}