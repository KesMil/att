﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IAuthorizer<in T> where T : notnull
    {
        Task<bool> Authorize(T request);
    }
}
