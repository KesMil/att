﻿using Application.Common.Interfaces;
using Application.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Common.Services
{
    public class AuthorizerService : IAuthorizerService
    {
        private readonly IDependencyResolver dependencyResolver;

        public AuthorizerService(IDependencyResolver dependencyResolver)
        {
            this.dependencyResolver = dependencyResolver;
        }

        public async Task<bool> Authorize<T>(T request) where T : class
        {
            var authorizers = this.dependencyResolver.Resolve<IEnumerable<IAuthorizer<T>>>();

            foreach (var authorizer in authorizers)
            {
                if(!await authorizer.Authorize(request))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
