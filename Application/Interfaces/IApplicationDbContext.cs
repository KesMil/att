﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IApplicationDbContext
    {
        DbSet<TestItem> TestItems { get; set; }
        DbSet<SiteItem> SiteItems { get; set; }
        DbSet<SiteLinkItem> SiteLinkItems { get; set; }
        DbSet<ClientCode> ClientCode { get; set; }
        DbSet<CWVInfo> CWVInfo { get; set; }
        DbSet<ReferenceClientCode> ReferenceClientCode { get; set; }
        DbSet<DiffIgnoreRegex> DiffIgnoreRegexes { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}