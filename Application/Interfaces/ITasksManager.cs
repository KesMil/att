﻿using Application.Common.BackgroundTasks.Interfaces;

namespace Application.Interfaces
{
    public interface ITasksManager
    {
        void EnqueueTask<T>(T backgroundTask) where T : IBackgroundTask;
    }
}
