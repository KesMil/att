﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Interfaces
{
    public interface IDependencyResolver
    {
        T Resolve<T>();
    }
}
