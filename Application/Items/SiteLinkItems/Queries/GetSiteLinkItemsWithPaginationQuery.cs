﻿using Application.Common.Mappings;
using Application.Common.Models;
using Application.DTOs;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Items.SiteLinkItems.Queries
{
    public class GetSiteLinkItemsWithPaginationQuery : IRequest<PaginatedList<SiteLinkItemDto>>
    {
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 10;
        public int SiteItemId { get; set; }

        public GetSiteLinkItemsWithPaginationQuery(int siteItemId)
        {
            SiteItemId = siteItemId;
        }

        public class GetSiteLinkItemsWithPaginationQueryHandler : IRequestHandler<GetSiteLinkItemsWithPaginationQuery, PaginatedList<SiteLinkItemDto>>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;
            private readonly ICurrentUserService _currentUserService;

            public GetSiteLinkItemsWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper, ICurrentUserService currentUserService)
            {
                _context = context;
                _mapper = mapper;
                _currentUserService = currentUserService;
            }

            public async Task<PaginatedList<SiteLinkItemDto>> Handle(GetSiteLinkItemsWithPaginationQuery request, CancellationToken cancellationToken)
            {
                return await _context.SiteLinkItems
                    .Where(x => x.CreatedBy == _currentUserService.UserId && x.SiteItemId == request.SiteItemId)
                    .OrderBy(x => x.Url)
                    .ProjectTo<SiteLinkItemDto>(_mapper.ConfigurationProvider)
                    .PaginatedListAsync(request.PageNumber, request.PageSize);
            }
        }
    }
}
