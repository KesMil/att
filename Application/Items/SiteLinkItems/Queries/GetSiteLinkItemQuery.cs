﻿using Application.DTOs;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Items.SiteLinkItems.Queries
{
    public class GetSiteLinkItemQuery : IRequest<SiteLinkItemDto>
    {
        public int Id { get; set; } = 0;
        public GetSiteLinkItemQuery(int siteLinkItemId)
        {
            Id = siteLinkItemId;
        }

        public class GetSiteLinkItemQueryHandler : IRequestHandler<GetSiteLinkItemQuery, SiteLinkItemDto>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetSiteLinkItemQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<SiteLinkItemDto> Handle(GetSiteLinkItemQuery request, CancellationToken cancellationToken)
            {
                return await Task.Run(() =>
                {
                    return _context.SiteLinkItems
                    .Where(x => x.Id == request.Id)
                    .ProjectTo<SiteLinkItemDto>(_mapper.ConfigurationProvider)
                    .FirstOrDefault();
                });
            }
        }
    }
}
