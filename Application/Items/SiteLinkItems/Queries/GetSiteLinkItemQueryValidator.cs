﻿using Application.Interfaces;
using Domain.Common;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Items.SiteLinkItems.Queries
{
    public class GetSiteLinkItemQueryValidator : AbstractValidator<GetSiteLinkItemQuery>
    {
        public GetSiteLinkItemQueryValidator(IApplicationDbContext context, ICurrentUserService currentUser)
        {
            RuleFor(siteLinkItem => siteLinkItem).Must((si, token) =>
            {
                var result = context.SiteLinkItems.Where(x => x.Id == si.Id && x.CreatedBy == currentUser.UserId).FirstOrDefault();
                if (result != null)
                    return true;
                return false;
            });
        }
    }
}
