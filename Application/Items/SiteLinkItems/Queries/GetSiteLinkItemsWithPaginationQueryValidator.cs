﻿using FluentValidation;
using System.Collections.Generic;
using System.Text;

namespace Application.Items.SiteLinkItems.Queries
{
    public class GetSiteLinkItemsWithPaginationQueryValidator : AbstractValidator<GetSiteLinkItemsWithPaginationQuery>
    {
        public GetSiteLinkItemsWithPaginationQueryValidator()
        {
            RuleFor(x => x.PageNumber)
                .GreaterThanOrEqualTo(1).WithMessage("PageNumber at least greater than or equal to 1.");

            RuleFor(x => x.PageSize)
                .GreaterThanOrEqualTo(1).WithMessage("PageSize at least greater than or equal to 1.");
        }
    }
}
