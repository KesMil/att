﻿using Application.Interfaces;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Items.SiteLinkItems.Commands.DeleteSiteLinkItem
{
    public class DeleteSiteLinkItemCommand : IRequest<bool>
    {
        public int Id { get; set; }

        public DeleteSiteLinkItemCommand(int id)
        {
            this.Id = id;
        }
    }

    public class EditSiteLinkCommandHandler : IRequestHandler<DeleteSiteLinkItemCommand, bool>
    {
        private readonly IApplicationDbContext _context;

        public EditSiteLinkCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<bool> Handle(DeleteSiteLinkItemCommand request, CancellationToken cancellationToken)
        {
            if (request.Id > 0)
            {
                var existingSiteLinkItem = await _context.SiteLinkItems.FindAsync(request.Id);

                if (existingSiteLinkItem == null)
                {
                    //TODO: throw new NotFoundException(nameof(TodoList), request.Id);
                    return false;
                }

                _context.SiteLinkItems.Remove(existingSiteLinkItem);
                await _context.SaveChangesAsync(cancellationToken);
                return true;
            }

            return false;
        }
    }
}
