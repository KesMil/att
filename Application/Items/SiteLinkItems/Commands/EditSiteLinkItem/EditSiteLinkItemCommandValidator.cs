﻿using Application.Interfaces;
using Application.Items.SiteItems.Queries;
using FluentValidation;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Application.Items.SiteLinkItems.Commands.EditSiteLinkItem
{
    public class EditSiteLinkItemCommandValidator : AbstractValidator<EditSiteLinkItemCommand>
    {
        public EditSiteLinkItemCommandValidator(IApplicationDbContext context, ICurrentUserService currentUser)
        {
            RuleFor(v => v.Url)
                .MaximumLength(200)
                .NotEmpty();

            RuleFor(siteLinkItem => siteLinkItem).Must((sli, token) =>
            {
                if (sli.Id > 0)
                {
                    var result = context.SiteLinkItems.Where(x => x.Id == sli.Id && x.CreatedBy == currentUser.UserId).FirstOrDefault();
                    if (result != null)
                        return true;
                    return false;
                }

                return true;
            });
        }
    }
}
