﻿using Application.Interfaces;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Items.SiteLinkItems.Commands.EditSiteLinkItem
{
    public class EditSiteLinkItemCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int SiteItemId { get; set; }
    }

    public class EditSiteLinkCommandHandler : IRequestHandler<EditSiteLinkItemCommand, int>
    {
        private readonly IApplicationDbContext _context;

        public EditSiteLinkCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(EditSiteLinkItemCommand request, CancellationToken cancellationToken)
        {
            var resultId = 0;

            // Update existing
            if (request.Id > 0)
            {
                var existingSiteLinkItem = await _context.SiteLinkItems.FindAsync(request.Id);

                if (existingSiteLinkItem == null)
                {
                    //TODO: throw new NotFoundException(nameof(TodoList), request.Id);
                }

                existingSiteLinkItem.Url = request.Url;
                resultId = existingSiteLinkItem.Id;
                await _context.SaveChangesAsync(cancellationToken);
            }

            // Create a new one
            if (request.SiteItemId > 0 && request.Id == 0)
            {
                var entity = new SiteLinkItem
                {
                    Url = request.Url,
                    SiteItemId = request.SiteItemId
                };
                _context.SiteLinkItems.Add(entity);
                await _context.SaveChangesAsync(cancellationToken);
                resultId = entity.Id;
            }

            return resultId;
        }
    }
}
