﻿using Application.Common.Interfaces;
using Application.Interfaces;
using System.Threading.Tasks;

namespace Application.Items.SiteLinkItems.Commands.EditSiteLinkItem
{
    public class EditSiteLinkItemCommandAuthorizer : IAuthorizer<EditSiteLinkItemCommand>
    {
        private readonly IApplicationDbContext context;
        private readonly ICurrentUserService currentUser;

        public EditSiteLinkItemCommandAuthorizer(IApplicationDbContext context, ICurrentUserService currentUser)
        {
            this.context = context;
            this.currentUser = currentUser;
        }

        public Task<bool> Authorize(EditSiteLinkItemCommand request)
        {
            var siteLinkItem = context.SiteLinkItems.Find(request.Id);
            if (siteLinkItem != null && siteLinkItem.CreatedBy == currentUser.UserId)
                return Task.FromResult(true);

            return Task.FromResult(false);
        }
    }
}
