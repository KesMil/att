﻿using Application.DTOs;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Items.CWVInfo.Queries
{
    public class GetCWVInfosQuery : IRequest<List<CWVInfoDto>>
    {
        public int SiteItemId { get; set; }
        public int ResultsLimit { get; set; }

        public GetCWVInfosQuery(int siteItemId, int limit = 100)
        {
            SiteItemId = siteItemId;
            ResultsLimit = limit;
        }

        public class GetCWVInfosQueryHandler : IRequestHandler<GetCWVInfosQuery, List<CWVInfoDto>>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetCWVInfosQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<List<CWVInfoDto>> Handle(GetCWVInfosQuery request, CancellationToken cancellationToken)
            {
                return await Task.Run(() =>
                {
                    var result = new List<CWVInfoDto>();

                    var ids = _context.SiteLinkItems.Where(sli => sli.SiteItemId == request.SiteItemId).Select(sli=>sli.Id).ToList();
                    foreach (var id in ids)
                    {
                        // TODO: Retrieve cwv data
                        var linkClientCodes = _context.CWVInfo
                        .Where(x => x.SiteLinkItemId == id)
                        .OrderByDescending(x => x.Created)
                        .Take(request.ResultsLimit)
                        .ProjectTo<CWVInfoDto>(_mapper.ConfigurationProvider)
                        .ToList();

                        result.AddRange(linkClientCodes);
                    }

                    return result;
                });
            }
        }
    }
}
