﻿using Application.DTOs;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Items.CWVInfo.Queries
{
    public class GetLatestCWVInfoByLinkIdQuery : IRequest<CWVInfoDto>
    {
        public int SiteLinkItemId { get; set; }

        public GetLatestCWVInfoByLinkIdQuery(int siteLinkItemId)
        {
            SiteLinkItemId = siteLinkItemId;
        }

        public class GetCWVInfoByLinkIdQueryHandler : IRequestHandler<GetLatestCWVInfoByLinkIdQuery, CWVInfoDto>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetCWVInfoByLinkIdQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<CWVInfoDto> Handle(GetLatestCWVInfoByLinkIdQuery request, CancellationToken cancellationToken)
            {
                return await Task.FromResult(
                    _context.CWVInfo
                        .Where(x => x.SiteLinkItemId == request.SiteLinkItemId)
                        .ProjectTo<CWVInfoDto>(_mapper.ConfigurationProvider)
                        .FirstOrDefault()
                        );
            }
        }
    }
}
