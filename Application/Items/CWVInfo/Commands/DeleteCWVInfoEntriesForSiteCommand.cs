﻿using Application.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Items.CWVInfo.Commands
{

    public class DeleteCWVInfoEntriesForSiteCommand : IRequest<bool>
    {
        public DeleteCWVInfoEntriesForSiteCommand(int siteId)
        {
            SiteId = siteId;
        }
        public int SiteId { get; set; }
    }

    public class DeleteCWVInfoEntriesForSiteCommandHandler : IRequestHandler<DeleteCWVInfoEntriesForSiteCommand, bool>
    {
        private readonly IApplicationDbContext _context;

        public DeleteCWVInfoEntriesForSiteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<bool> Handle(DeleteCWVInfoEntriesForSiteCommand request, CancellationToken cancellationToken)
        {
            var existingSiteItem = await _context.SiteItems.FindAsync(request.SiteId);

            if (existingSiteItem == null)
            {
                return await Task.FromResult(false);
            }

            var cwvScansToRemove = _context.CWVInfo.Where(c => c.SiteLinkItem.SiteItemId == request.SiteId);
            _context.CWVInfo.RemoveRange(cwvScansToRemove);

            await _context.SaveChangesAsync(cancellationToken);

            return await Task.FromResult(true);
        }
    }
}
