﻿using Application.Common.BackgroundTasks.Tasks;
using Application.DTOs;
using Application.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Items.CWVInfo.Commands
{

    public class InitCWVScanCommand : IRequest<bool>
    {
        public InitCWVScanCommand(List<SiteLinkItemDto> siteLinkItemDtos, string host)
        {
            SiteLinkItemDtos = siteLinkItemDtos;
            Host = host;
        }
        public List<SiteLinkItemDto> SiteLinkItemDtos { get; }
        public string Host { get; }
    }

    public class InitCWVScanCommandHandler : IRequestHandler<InitCWVScanCommand, bool>
    {
        private readonly IApplicationDbContext _context;
        private readonly ITasksManager _tasksManager;

        public InitCWVScanCommandHandler(IApplicationDbContext context, ITasksManager tasksManager)
        {
            _context = context;
            _tasksManager = tasksManager;
        }

        public async Task<bool> Handle(InitCWVScanCommand request, CancellationToken cancellationToken)
        {
            foreach (var link in request.SiteLinkItemDtos)
            {
                var fullUrl = request.Host + link.Url;
                // Create new CWV data container object
                var CWVInfoRecord = new Domain.Entities.CWVInfo()
                {
                    UrlUsed = fullUrl,
                    SiteLinkItemId = link.Id,
                    IsMetricScanCompleted = false
                };

                // Store container in database
                _context.CWVInfo.Add(CWVInfoRecord);
                await _context.SaveChangesAsync(new System.Threading.CancellationToken());

                // Init new task to perform the CWV scan
                ScanCoreWebVitalsTask task = new ScanCoreWebVitalsTask() { CWVInfoId = CWVInfoRecord.Id, Url = fullUrl };
                _tasksManager.EnqueueTask(task);
            }

            return true;
        }
    }
}
