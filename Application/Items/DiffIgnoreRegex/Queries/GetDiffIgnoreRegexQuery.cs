﻿using Application.DTOs;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Items.DiffIgnoreRegex.Queries
{
    public class GetDiffIgnoreRegexQuery : IRequest<DiffIgnoreRegexDto>
    {
        public int siteItemId { get; set; }

        public GetDiffIgnoreRegexQuery(int siteItemId)
        {
            this.siteItemId = siteItemId;
        }

        public class GetDiffIgnoreRegexQueryHandler : IRequestHandler<GetDiffIgnoreRegexQuery, DiffIgnoreRegexDto>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetDiffIgnoreRegexQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<DiffIgnoreRegexDto> Handle(GetDiffIgnoreRegexQuery request, CancellationToken cancellationToken)
            {
                return await Task.Run(() =>
                {
                    DiffIgnoreRegexDto result = _context.DiffIgnoreRegexes
                        .Where(x => x.SiteItemId == request.siteItemId)
                        .ProjectTo<DiffIgnoreRegexDto>(_mapper.ConfigurationProvider).FirstOrDefault();

                    return result;
                });
            }
        }
    }
}
