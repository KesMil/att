﻿using Application.Interfaces;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Items.DiffIgnoreRegex.Commands
{
    public class EditIgnoreRegexCommand : IRequest<int>
    {
        public int SiteItemId { get; set; }
        public string RegexLines { get; set; }
    }

    public class EditDiffIgnoreRegexCommandHandler : IRequestHandler<EditIgnoreRegexCommand, int>
    {
        private readonly IApplicationDbContext _context;

        public EditDiffIgnoreRegexCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(EditIgnoreRegexCommand request, CancellationToken cancellationToken)
        {
            var updatedEntity = new Domain.Entities.DiffIgnoreRegex
            {
                SiteItemId = request.SiteItemId,
                RegexLines = request.RegexLines
            };

            // Get existing by siteId, because only one can be created per site.
            var existingEntity = _context.DiffIgnoreRegexes.Where(dir=>dir.SiteItemId== request.SiteItemId).FirstOrDefault();
            updatedEntity.Id = existingEntity?.Id ?? 0;

            if (updatedEntity.Id == 0)
            {
                _context.DiffIgnoreRegexes.Add(updatedEntity);
            }
            else
            {
                existingEntity.RegexLines = updatedEntity.RegexLines;
            }

            await _context.SaveChangesAsync(cancellationToken);

            return updatedEntity.Id;
        }
    }
}
