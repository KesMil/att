﻿using Application.Interfaces;
using Application.Items.SiteItems.Queries;
using FluentValidation;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Application.Items.SiteItems.Commands.EditSiteItem
{
    public class EditSiteItemCommandValidator : AbstractValidator<EditSiteItemCommand>
    {
        public EditSiteItemCommandValidator(IApplicationDbContext context, ICurrentUserService currentUser)
        {
            RuleFor(v => v.Title)
                .MaximumLength(200)
                .NotEmpty();

            RuleFor(v => v.Host)
                .MaximumLength(200)
                .NotEmpty()
                .Matches(@"^(http:\/\/|https:\/\/)([a-z\-]+)?:?(\/\/)?([a-zA-Z0-9\.]+)?(:\d+)?$").WithMessage("Must be valid host.");

            RuleFor(siteItem => siteItem).Must((si, token) =>
            {
                if (si.Id > 0)
                {
                    var result = context.SiteItems.Where(x => x.Id == si.Id && x.CreatedBy == currentUser.UserId).FirstOrDefault();
                    if (result != null)
                        return true;
                    return false;
                }

                return true;
            });
        }
    }
}
