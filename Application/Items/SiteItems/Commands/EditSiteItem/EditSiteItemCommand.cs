﻿using Application.Interfaces;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Items.SiteItems.Commands.EditSiteItem
{
    public class EditSiteItemCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Host { get; set; }
    }

    public class EditSiteItemCommandHandler : IRequestHandler<EditSiteItemCommand, int>
    {
        private readonly IApplicationDbContext _context;

        public EditSiteItemCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(EditSiteItemCommand request, CancellationToken cancellationToken)
        {
            var entity = new SiteItem
            {
                Id = request.Id,
                Title = request.Title,
                Host = request.Host
            };

            if (entity.Id == 0)
            {
                _context.SiteItems.Add(entity);
            }
            else
            {
                var existingSiteItem = await _context.SiteItems.FindAsync(entity.Id);

                if (existingSiteItem == null)
                {
                    //TODO: throw new NotFoundException(nameof(TodoList), request.Id);
                }

                existingSiteItem.Title = entity.Title;
                existingSiteItem.Host = entity.Host;
            }

            await _context.SaveChangesAsync(cancellationToken);

            return entity.Id;
        }
    }
}
