﻿using Application.Common.Interfaces;
using Application.Interfaces;
using System;
using System.Threading.Tasks;

namespace Application.Items.SiteItems.Commands.EditSiteItem
{
    public class EditSiteItemCommandAuthorizer : IAuthorizer<EditSiteItemCommand>
    {
        private readonly IApplicationDbContext context;
        private readonly ICurrentUserService currentUser;

        public EditSiteItemCommandAuthorizer(IApplicationDbContext context, ICurrentUserService currentUser)
        {
            this.context = context;
            this.currentUser = currentUser;
        }

        public Task<bool> Authorize(EditSiteItemCommand request)
        {
            if (request == null || request.Id == 0)
                return Task.FromResult(true);

            var siteItem = context.SiteItems.Find(request.Id);
            if (siteItem != null && siteItem.CreatedBy == currentUser.UserId)
                return Task.FromResult(true);

            return Task.FromResult(false);
        }
    }
}
