﻿using Application.Interfaces;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Items.SiteItems.Commands.DeleteSiteItem
{
    public class DeleteSiteItemCommand : IRequest<int>
    {
        public int Id { get; set; }
    }

    public class DeleteSiteItemCommandHandler : IRequestHandler<DeleteSiteItemCommand, int>
    {
        private readonly IApplicationDbContext _context;

        public DeleteSiteItemCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(DeleteSiteItemCommand request, CancellationToken cancellationToken)
        {
            var existingSiteItem = await _context.SiteItems.FindAsync(request.Id);

            if (existingSiteItem == null)
            {
                return await Task.FromResult(0);
            }

            _context.SiteItems.Remove(existingSiteItem);

            await _context.SaveChangesAsync(cancellationToken);

            return existingSiteItem.Id;
        }
    }
}
