﻿using Application.Common.Mappings;
using Application.Common.Models;
using Application.DTOs;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Items.SiteItems.Queries
{
    public class GetSiteItemsWithPaginationQuery : IRequest<PaginatedList<SiteItemDto>>
    {
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 10;

        public class GetSiteItemsWithPaginationQueryHandler : IRequestHandler<GetSiteItemsWithPaginationQuery, PaginatedList<SiteItemDto>>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;
            private readonly ICurrentUserService _currentUserService;

            public GetSiteItemsWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper, ICurrentUserService currentUserService)
            {
                _context = context;
                _mapper = mapper;
                _currentUserService = currentUserService;
            }

            public async Task<PaginatedList<SiteItemDto>> Handle(GetSiteItemsWithPaginationQuery request, CancellationToken cancellationToken)
            {
                return await _context.SiteItems
                    .Where(x => x.CreatedBy == _currentUserService.UserId)
                    .OrderBy(x => x.Title)
                    .ProjectTo<SiteItemDto>(_mapper.ConfigurationProvider)
                    .PaginatedListAsync(request.PageNumber, request.PageSize);
            }
        }
    }
}
