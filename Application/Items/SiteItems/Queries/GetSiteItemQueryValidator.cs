﻿using Application.Interfaces;
using Domain.Common;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Items.SiteItems.Queries
{
    public class GetSiteItemQueryValidator : AbstractValidator<GetSiteItemQuery>
    {
        public GetSiteItemQueryValidator(IApplicationDbContext context, ICurrentUserService currentUser)
        {
            RuleFor(siteItem => siteItem).Must((si, token) =>
            {
                var result = context.SiteItems.Where(x => x.Id == si.Id && x.CreatedBy == currentUser.UserId).FirstOrDefault();
                if (result != null)
                    return true;
                return false;
            });
        }
    }
}
