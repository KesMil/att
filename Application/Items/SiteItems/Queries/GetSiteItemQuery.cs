﻿using Application.DTOs;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Items.SiteItems.Queries
{
    public class GetSiteItemQuery : IRequest<SiteItemDto>
    {
        public int Id { get; set; } = 0;
        public GetSiteItemQuery(int siteItemId)
        {
            Id = siteItemId;
        }

        public class GetSiteItemQueryHandler : IRequestHandler<GetSiteItemQuery, SiteItemDto>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetSiteItemQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<SiteItemDto> Handle(GetSiteItemQuery request, CancellationToken cancellationToken)
            {
                return await Task.Run(() =>
                {
                    return _context.SiteItems
                    .Where(x => x.Id == request.Id)
                    .ProjectTo<SiteItemDto>(_mapper.ConfigurationProvider)
                    .FirstOrDefault();
                });
            }
        }
    }
}
