﻿using Application.Common.BackgroundTasks.Enums;
using Application.DTOs;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Items.ClientCode.Commands
{
    public class DeleteClientCodesBySiteIdCommand : IRequest<bool>
    {
        public DeleteClientCodesBySiteIdCommand(int siteId, ClientCodeRequestMode stateToDelete)
        {
            SiteId = siteId;
            StateToDelete = stateToDelete;
        }
        public int SiteId { get; set; }
        public ClientCodeRequestMode StateToDelete { get; }
    }

    public class DeleteClientCodesBySiteIdCommandHandler : IRequestHandler<DeleteClientCodesBySiteIdCommand, bool>
    {
        private readonly IApplicationDbContext _context;

        public DeleteClientCodesBySiteIdCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<bool> Handle(DeleteClientCodesBySiteIdCommand request, CancellationToken cancellationToken)
        {
            IQueryable<Domain.Entities.ClientCode> primaryCodes = _context.ReferenceClientCode.Where(rcc => rcc.ClientCode.SiteLinkItem.SiteItemId == request.SiteId).Select(rcc=>rcc.ClientCode);
            var secondaryCodes = _context.ClientCode.Where(cc => !primaryCodes.Contains(cc));

            if (request.StateToDelete == ClientCodeRequestMode.CurrentState)
            {
                _context.ClientCode.RemoveRange(primaryCodes);
            } else
            {
                _context.ClientCode.RemoveRange(secondaryCodes);
            }

            await _context.SaveChangesAsync(cancellationToken);

            return true;
        }
    }
}
