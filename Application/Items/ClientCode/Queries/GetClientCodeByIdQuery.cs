﻿using Application.DTOs;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Items.ClientCode.Queries
{
    public class GetClientCodeByIdQuery : IRequest<ClientCodeDto>
    {
        public int clientCodeId { get; set; }

        public GetClientCodeByIdQuery(int clientCodeId)
        {
            this.clientCodeId = clientCodeId;
        }

        public class GetClientCodeByIdQueryHandler : IRequestHandler<GetClientCodeByIdQuery, ClientCodeDto>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetClientCodeByIdQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<ClientCodeDto> Handle(GetClientCodeByIdQuery request, CancellationToken cancellationToken)
            {
                return await Task.Run(() =>
                {
                    ClientCodeDto result = _context.ClientCode
                        .Where(x => x.Id == request.clientCodeId)
                        .ProjectTo<ClientCodeDto>(_mapper.ConfigurationProvider).FirstOrDefault();

                    return result;
                });
            }
        }
    }
}
