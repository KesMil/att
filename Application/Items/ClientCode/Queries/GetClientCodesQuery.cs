﻿using Application.DTOs;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Items.ClientCode.Queries
{
    public class GetClientCodesQuery : IRequest<List<ClientCodeDto>>
    {
        public int SiteItemId { get; set; }
        public int LimitOfCodesPerLink { get; set; }

        public GetClientCodesQuery(int siteItemId, int limitOfCodesPerLink = 100)
        {
            SiteItemId = siteItemId;
            LimitOfCodesPerLink = limitOfCodesPerLink;
        }

        public class GetClientCodesQueryHandler : IRequestHandler<GetClientCodesQuery, List<ClientCodeDto>>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetClientCodesQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<List<ClientCodeDto>> Handle(GetClientCodesQuery request, CancellationToken cancellationToken)
            {
                return await Task.Run(() =>
                {
                    var result = new List<ClientCodeDto>();

                    var ids = _context.SiteLinkItems.Where(sli => sli.SiteItemId == request.SiteItemId).Select(sli=>sli.Id).ToList();
                    foreach (var id in ids)
                    {
                        var linkClientCodes = _context.ClientCode
                        .Where(x => x.SiteLinkItemId == id)
                        .OrderByDescending(x => x.Created)
                        .Take(request.LimitOfCodesPerLink)
                        .ProjectTo<ClientCodeDto>(_mapper.ConfigurationProvider)
                        .ToList();

                        result.AddRange(linkClientCodes);
                    }

                    return result;
                });
            }
        }
    }
}
