﻿using Application.DTOs;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Items.ClientCode.Queries
{
    public class GetReferenceClientCodesQuery : IRequest<List<ClientCodeDto>>
    {
        public int SiteItemId { get; set; }
        public int LimitOfCodesPerLink { get; set; }

        public GetReferenceClientCodesQuery(int siteItemId)
        {
            SiteItemId = siteItemId;
        }

        public class GetReferenceClientCodesQueryHandler : IRequestHandler<GetReferenceClientCodesQuery, List<ClientCodeDto>>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetReferenceClientCodesQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<List<ClientCodeDto>> Handle(GetReferenceClientCodesQuery request, CancellationToken cancellationToken)
            {
                return await Task.Run(() =>
                {
                    var result = new List<ClientCodeDto>();

                    var ids = _context.SiteLinkItems.Where(sli => sli.SiteItemId == request.SiteItemId).Select(sli => sli.Id).ToList();
                    foreach (var id in ids)
                    {
                        ClientCodeDto rcc = _context.ReferenceClientCode.Where(rcc => rcc.SiteLinkItemId == id).Select(rcc=>rcc.ClientCode).ProjectTo<ClientCodeDto>(_mapper.ConfigurationProvider).FirstOrDefault();
                        if (rcc != null)
                            result.Add(rcc);
                    }

                    return result;
                });
            }
        }
    }
}
