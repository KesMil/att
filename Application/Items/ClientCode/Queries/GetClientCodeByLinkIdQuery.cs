﻿using Application.DTOs;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Items.ClientCode.Queries
{
    public class GetClientCodeByLinkIdQuery : IRequest<ClientCodeDto>
    {
        public int SiteLinkItemId { get; set; }

        public GetClientCodeByLinkIdQuery(int siteLinkItemId)
        {
            SiteLinkItemId = siteLinkItemId;
        }

        public class GetClientCodeByLinkIdQueryHandler : IRequestHandler<GetClientCodeByLinkIdQuery, ClientCodeDto>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetClientCodeByLinkIdQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<ClientCodeDto> Handle(GetClientCodeByLinkIdQuery request, CancellationToken cancellationToken)
            {
                return await Task.FromResult(
                    _context.ClientCode
                        .Where(x => x.SiteLinkItemId == request.SiteLinkItemId)
                        .ProjectTo<ClientCodeDto>(_mapper.ConfigurationProvider)
                        .FirstOrDefault()
                        );
            }
        }
    }
}
