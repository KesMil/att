﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebUI.ViewModels.DiffTool
{
    public class DiffViewViewModel
    {
        public string ReferenceContent { get; set; }
        public string CompareToContent { get; set; }
    }
}
