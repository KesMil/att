﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebUI.ViewModels.DiffTool
{
    public class SiteLinkItemDiffDetailsViewModel
    {
        public int LinkId { get; set; }
        public int SiteId { get; set; }
        public string Url { get; set; }
        public List<ClientCodeListEntryViewModel> AllClientCodes { get; set; }
        public ClientCodeListEntryViewModel ReferenceClientCode { get; set; }
    }
}
