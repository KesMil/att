﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebUI.ViewModels.Sites;

namespace WebUI.ViewModels.DiffTool
{
    public class DiffToolMainViewModel
    {
        public SiteItemDetailsViewModel SiteItemDetails { get; set; }
        public List<SiteLinkItemDiffDetailsViewModel> Links { get; set; }
        [DataType(DataType.MultilineText)]
        public string DiffIgnoreRegex { get; set; }
    }
}
