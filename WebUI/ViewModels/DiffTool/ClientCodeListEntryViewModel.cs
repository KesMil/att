﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebUI.ViewModels.DiffTool
{
    public class ClientCodeListEntryViewModel
    {
        public int Id { get; set; }
        public int SiteLinkItemId { get; set; }
        public string UrlUsed { get; set; }
        public bool IsDownloadingCompleted { get; set; }
        public bool IsDifferentFromReferenceCode{ get; set; }
        public int? ResponseCode { get; set; }
    }
}
