﻿using Application.DTOs;
using Application.Items.SiteItems.Commands.EditSiteItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebUI.ViewModels.Sites
{
    public class SiteEditViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Host { get; set; }
    }
}
