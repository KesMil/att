﻿using System.ComponentModel.DataAnnotations;

namespace WebUI.ViewModels.Sites
{
    public class SiteLinkItemDetailsViewModel
    {
        public int LinkId { get; set; }
        public int SiteId { get; set; }
        [Required]
        [RegularExpression(@"(\/)(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?", ErrorMessage = "Must be valid url relative to the host.")]
        [MaxLength(300)]
        public string Url { get; set; }
    }
}