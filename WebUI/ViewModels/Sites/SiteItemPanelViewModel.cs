﻿using Application.Common.Models;
using Application.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebUI.ViewModels.Sites
{
    public class SiteItemPanelViewModel
    {
        public SiteItemDetailsViewModel SiteItemDetails { get; set; }
        public List<SiteLinkItemDetailsViewModel> Links { get; set; }
        public SiteEditViewModel SiteEditViewModel { get; set; }
    }
}