﻿using Application.Common.Models;
using Application.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebUI.ViewModels.Sites
{
    public class SiteItemDetailsViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Host { get; set; }
    }
}