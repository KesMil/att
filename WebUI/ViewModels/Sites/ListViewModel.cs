﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebUI.ViewModels.Sites
{
    public class ListViewModel
    {
        public List<SiteItemDetailsViewModel> Sites { get; set; }
        public int TotalSites { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
    }
}