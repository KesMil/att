﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebUI.ViewModels.CoreWebVitalsTool
{
    public class LinkCWVInfoViewModel
    {
        public int LinkId { get; set; }
        public int SiteId { get; set; }
        public string Url { get; set; }
        public CWVEntryViewModel cwvEntryViewModel { get; set; }
    }
}