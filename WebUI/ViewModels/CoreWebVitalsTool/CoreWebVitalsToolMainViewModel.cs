﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebUI.ViewModels.Sites;

namespace WebUI.ViewModels.CoreWebVitalsTool
{
    public class CoreWebVitalsToolMainViewModel
    {
        public SiteItemDetailsViewModel SiteItemDetails { get; set; }
        public List<LinkCWVInfoViewModel> Links { get; set; }
    }
}
