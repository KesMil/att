﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebUI.ViewModels.CoreWebVitalsTool
{
    public class CWVEntryViewModel
    {
        public int Id { get; set; }
        public int SiteLinkItemId { get; set; }
        public string UrlUsed { get; set; }
        public int Performance { get; set; }
        public bool IsScanSuccessful { get; set; }
        public bool IsScanCompleted { get; set; }
        public int SEO { get; set; }
        public int BestPractices { get; set; }
        public int Accessibility { get; set; }

        // TODO: all core web vitals props here that are needed on view
    }
}
