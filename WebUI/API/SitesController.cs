﻿using Application.Common.Interfaces;
using Application.DTOs;
using Application.Items.SiteLinkItems.Commands.EditSiteLinkItem;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebUI.ViewModels.Sites;
using WebUI.Extensions;
using Application.Items.SiteLinkItems.Queries;
using Application.Items.SiteLinkItems.Commands.DeleteSiteLinkItem;
using Application.Items.SiteItems.Commands.DeleteSiteItem;

namespace WebUI.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class SitesController : WebUI.Controllers.ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IAuthorizerService authorizerService;

        public SitesController(IMapper mapper, IAuthorizerService authorizerService)
        {
            this.mapper = mapper;
            this.authorizerService = authorizerService;
        }

        [HttpPost]
        public async Task<IActionResult> EditSiteLink([FromForm] SiteLinkItemDetailsViewModel model)
        {
            EditSiteLinkItemCommand command = model.ToEditCommand();
            if (ModelState.IsValid)
            {
                var id = await Mediator.Send(command);
                GetSiteLinkItemQuery query = new GetSiteLinkItemQuery(id);
                SiteLinkItemDetailsViewModel returnModel = new SiteLinkItemDetailsViewModel();
                returnModel.MapFrom(await Mediator.Send(query));
                return Ok(returnModel);
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        public async Task<IActionResult> RemoveSiteLink([FromForm] int id)
        {
            DeleteSiteLinkItemCommand command = new DeleteSiteLinkItemCommand(id);
            if (ModelState.IsValid)
            {
                var sucess = await Mediator.Send(command);
                if (sucess)
                {
                    return Ok();
                } else
                {
                    return BadRequest();
                }
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPost]
        [Route("Delete/{siteId}")]
        public async Task<IActionResult> DeleteSiteItem(int siteId)
        {
            DeleteSiteItemCommand deleteSiteItemCommand = new DeleteSiteItemCommand() { Id = siteId };
            await Mediator.Send(deleteSiteItemCommand);
            return Ok();
        }
    }
}
