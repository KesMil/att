﻿using Application.Interfaces;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebUI.ViewModels.Sites;

namespace WebUI.ViewModelValidators
{
    public class SiteLinkItemDetailsViewModelValidator : AbstractValidator<SiteLinkItemDetailsViewModel>
    {
        public SiteLinkItemDetailsViewModelValidator(IApplicationDbContext context, ICurrentUserService currentUser)
        {
            RuleFor(v => v.Url)
                .MaximumLength(200)
                .NotEmpty();
        }
    }
}