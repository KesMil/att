﻿using Application.Interfaces;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebUI.ViewModels.Sites;

namespace WebUI.ViewModelValidators
{
    public class SiteEditViewModelValidator : AbstractValidator<SiteEditViewModel>
    {
        public SiteEditViewModelValidator(IApplicationDbContext context, ICurrentUserService currentUser)
        {
            RuleFor(v => v.Title)
                .MaximumLength(200)
                .NotEmpty();

            RuleFor(v => v.Host)
                .MaximumLength(200)
                .NotEmpty()
                .Matches(@"^(http:\/\/|https:\/\/)([a-z\-]+)?:?(\/\/)?([a-zA-Z0-9\.]+)?(:\d+)?$").WithMessage("Must be valid host.");

            RuleFor(siteItem => siteItem).Must((si, token) =>
            {
                if (si.Id > 0)
                {
                    var result = context.SiteItems.Where(x => x.Id == si.Id && x.CreatedBy == currentUser.UserId).FirstOrDefault();
                    if (result != null)
                        return true;
                    return false;
                }

                return true;
            });
        }
    }
}