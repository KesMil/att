﻿using Application.Common.BackgroundTasks.Tasks;
using Application.Common.Interfaces;
using Application.Common.Models;
using Application.DTOs;
using Application.Interfaces;
using Application.Items.CWVInfo.Commands;
using Application.Items.CWVInfo.Queries;
using Application.Items.SiteItems.Queries;
using Application.Items.SiteLinkItems.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WebUI.Extensions;
using WebUI.ViewModels.CoreWebVitalsTool;

namespace WebUI.Controllers
{
    public class CoreWebVitalsToolController : ControllerBase
    {
        private readonly IAuthorizerService authorizerService;
        private readonly IApplicationDbContext context;
        private readonly ITasksManager tasksManager;

        public CoreWebVitalsToolController(IAuthorizerService authorizerService, IApplicationDbContext context, ITasksManager tasksManager)
        {
            this.authorizerService = authorizerService;
            this.context = context;
            this.tasksManager = tasksManager;
        }

        [Route("cwvtool/{siteId}")]
        public async Task<IActionResult> Index(int siteId)
        {
            // Create query
            GetSiteItemQuery getSiteItemQuery = new GetSiteItemQuery(siteId);

            // Validate if query can be executed by this user
            if (!await this.authorizerService.Authorize<GetSiteItemQuery>(getSiteItemQuery))
                return NotFound();

            // Execute query to retrieve site data
            SiteItemDto existingSiteItem = await Mediator.Send(getSiteItemQuery);

            // Get list of all site links
            GetSiteLinkItemsWithPaginationQuery getSiteLinkItems = new GetSiteLinkItemsWithPaginationQuery(existingSiteItem.Id) { PageSize = 9999 }; // TODO: Create another Query without pages
            PaginatedList<SiteLinkItemDto> existingSiteLinkItems = await Mediator.Send(getSiteLinkItems);

            // Build model
            CoreWebVitalsToolMainViewModel viewModel = new CoreWebVitalsToolMainViewModel();
            viewModel.SiteItemDetails = new ViewModels.Sites.SiteItemDetailsViewModel();
            viewModel.SiteItemDetails.MapFrom(existingSiteItem);
            viewModel.Links = new List<LinkCWVInfoViewModel>();
            foreach (var link in existingSiteLinkItems.Items)
            {
                LinkCWVInfoViewModel linkCWVInfoViewModel = new LinkCWVInfoViewModel();
                GetLatestCWVInfoByLinkIdQuery getSingleCWVInfoQuery = new GetLatestCWVInfoByLinkIdQuery(link.Id);
                var cwvInfoDto = await Mediator.Send(getSingleCWVInfoQuery);
                linkCWVInfoViewModel.MapFrom(link, cwvInfoDto);
                viewModel.Links.Add(linkCWVInfoViewModel);
            }

            return View(viewModel);
        }

        [HttpPost]
        [Route("ScanVitals/{siteId}")]
        public async Task<IActionResult> ScanSiteVitals(int siteId)
        {
            // Get Site entry data
            GetSiteItemQuery getSiteItemQuery = new GetSiteItemQuery(siteId);
            if (!await this.authorizerService.Authorize<GetSiteItemQuery>(getSiteItemQuery))
                return NotFound();

            SiteItemDto existingSiteItem = await Mediator.Send(getSiteItemQuery);

            // Get Site Links
            // FYI - while pagination supported on Query, in the current application it is not used in user views, so we set default page size to 1000.
            GetSiteLinkItemsWithPaginationQuery getSiteLinkItems = new GetSiteLinkItemsWithPaginationQuery(siteId) { PageSize = 1000 };
            PaginatedList<SiteLinkItemDto> existingSiteLinkItems = await Mediator.Send(getSiteLinkItems);

            // WORK IN PROGRESS
            // TODO: This code is here temporarry until program evolves further and we start using historical scans data on the views.
            // Remove old CWV data
            // This old data could be stored for later analysis, but for performance and storage reasons we are removing old data.
            DeleteCWVInfoEntriesForSiteCommand deleteCWVInfoEntriesForSiteCommand = new DeleteCWVInfoEntriesForSiteCommand(siteId);
            await Mediator.Send(deleteCWVInfoEntriesForSiteCommand);
            // ---WORK IN PROGRESS

            // Init new CWV scan
            InitCWVScanCommand initCWVScanCommand = new InitCWVScanCommand(existingSiteLinkItems.Items, existingSiteItem.Host);
            await Mediator.Send(initCWVScanCommand);

            return RedirectToAction("Index", new { siteId });
        }


        [Route("ShowVitals/{id}")]
        [Produces("text/html")]
        public async Task<ActionResult<string>> ShowVitals(int id)
        {
            GetLatestCWVInfoByLinkIdQuery getSingleCWVInfoQuery = new GetLatestCWVInfoByLinkIdQuery(id);
            var result = await Mediator.Send(getSingleCWVInfoQuery);
            return Content(result.Source, "text/html");
        }
    }
}
