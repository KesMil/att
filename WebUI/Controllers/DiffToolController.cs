﻿using Application.Common.BackgroundTasks.Enums;
using Application.Common.BackgroundTasks.Tasks;
using Application.Common.Interfaces;
using Application.Common.Models;
using Application.DTOs;
using Application.Interfaces;
using Application.Items.ClientCode.Commands;
using Application.Items.ClientCode.Queries;
using Application.Items.DiffIgnoreRegex.Commands;
using Application.Items.DiffIgnoreRegex.Queries;
using Application.Items.SiteItems.Commands.DeleteSiteItem;
using Application.Items.SiteItems.Queries;
using Application.Items.SiteLinkItems.Queries;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WebUI.Extensions;
using WebUI.ViewModels.DiffTool;
using WebUI.ViewModels.Sites;

namespace WebUI.Controllers
{
    public class DiffToolController : ControllerBase
    {
        private readonly ILogger<SitesController> logger;
        private readonly IMapper mapper;
        private readonly IAuthorizerService authorizerService;
        private readonly ITasksManager tasksManager;
        private readonly IApplicationDbContext context;

        public DiffToolController(ILogger<SitesController> logger, IMapper mapper, IAuthorizerService authorizerService, ITasksManager tasksManager, IApplicationDbContext context)
        {
            this.logger = logger;
            this.mapper = mapper;
            this.authorizerService = authorizerService;
            this.tasksManager = tasksManager;
            this.context = context;
        }

        [Route("DiffTool/{siteId}")]
        public async Task<IActionResult> Index(int siteId)
        {
            GetSiteItemQuery getSiteItemQuery = new GetSiteItemQuery(siteId);
            if (!await this.authorizerService.Authorize<GetSiteItemQuery>(getSiteItemQuery))
                return NotFound();

            SiteItemDto existingSiteItem = await Mediator.Send(getSiteItemQuery);

            GetSiteLinkItemsWithPaginationQuery getSiteLinkItems = new GetSiteLinkItemsWithPaginationQuery(existingSiteItem.Id) { PageSize=9999 }; // TODO: Create another Query without pages
            PaginatedList<SiteLinkItemDto> existingSiteLinkItems = await Mediator.Send(getSiteLinkItems);

            GetClientCodesQuery getClientCodes = new GetClientCodesQuery(siteId);
            List<ClientCodeDto> clientCode = await Mediator.Send(getClientCodes);

            GetReferenceClientCodesQuery getReferenceClientCodes = new GetReferenceClientCodesQuery(siteId);
            List<ClientCodeDto> refClientCodes = await Mediator.Send(getReferenceClientCodes);

            GetDiffIgnoreRegexQuery getDiffIgnoreRegexQuery = new GetDiffIgnoreRegexQuery(siteId);
            DiffIgnoreRegexDto diffIgnoreRegexDto = await Mediator.Send(getDiffIgnoreRegexQuery);

            DiffToolMainViewModel viewModel = new DiffToolMainViewModel();
            viewModel.MapFrom(existingSiteItem, existingSiteLinkItems, clientCode, refClientCodes, diffIgnoreRegexDto);

            return View(viewModel);
        }

        [HttpPost]
        [Route("UpdateRegex/{siteId}")]
        public async Task<IActionResult> UpdateRegex([FromForm]string diffIgnoreRegex, int siteId)
        {
            EditIgnoreRegexCommand editIgnoreRegexCommand = new EditIgnoreRegexCommand() { RegexLines = diffIgnoreRegex, SiteItemId = siteId };
            await Mediator.Send(editIgnoreRegexCommand);
            return RedirectToAction("Index", new { siteId });
        }

        [HttpPost]
        [Route("ScanSite/{siteId}/{mode=0}")]
        public async Task<IActionResult> ScanSite(int siteId, int mode)
        {
            //TODO: use authorization service to check if user is allowed to execute queries and commands

            GetSiteItemQuery getSiteItemQuery = new GetSiteItemQuery(siteId);
            SiteItemDto siteItem = await Mediator.Send(getSiteItemQuery);

            GetSiteLinkItemsWithPaginationQuery getSiteLinkItems = new GetSiteLinkItemsWithPaginationQuery(siteId) { PageSize=9999 };
            PaginatedList<SiteLinkItemDto> existingSiteLinkItems = await Mediator.Send(getSiteLinkItems);

            // IN PROGRESS
            // Remove old scans for performance reasons, because right now historical records are not used in the application.
            DeleteClientCodesBySiteIdCommand deleteClientCodesBySiteIdCommand = new DeleteClientCodesBySiteIdCommand(siteId, (ClientCodeRequestMode)mode);
            await Mediator.Send(deleteClientCodesBySiteIdCommand);
            // ---IN PROGRESS

            // Initialise new source code scan.
            foreach (var link in existingSiteLinkItems.Items)
            {
                var fullUrl = siteItem.Host + link.Url;
                var clienCodeRecord = new Domain.Entities.ClientCode()
                {
                    IsDownloadingCompleted = false,
                    UrlUsed = fullUrl,
                    SiteLinkItemId = link.Id
                };

                // Create initial values in db (in progress db entries)
                // TODO: to be moved to QUERY instead
                this.context.ClientCode.Add(clienCodeRecord);
                await this.context.SaveChangesAsync(new System.Threading.CancellationToken());
                // Set current clients code IF Mode is set to set current site state
                if (mode == (int)ClientCodeRequestMode.CurrentState)
                {
                    var refClientCode = this.context.ReferenceClientCode.Where(rcc => rcc.SiteLinkItemId == clienCodeRecord.SiteLinkItemId).FirstOrDefault();

                    if (refClientCode == null)
                    {
                        refClientCode = new Domain.Entities.ReferenceClientCode() { ClientCode = clienCodeRecord, SiteLinkItemId = clienCodeRecord.SiteLinkItemId };
                        this.context.ReferenceClientCode.Add(refClientCode);
                    }
                    else
                    {
                        refClientCode.ClientCode = clienCodeRecord;
                        refClientCode.SiteLinkItemId = clienCodeRecord.SiteLinkItemId;
                    }
                }
                await this.context.SaveChangesAsync(new System.Threading.CancellationToken());

                // Create background task to download client codes.
                GetClientCodeTask task = new GetClientCodeTask() { ClientCodeId = clienCodeRecord.Id, Url = fullUrl };
                tasksManager.EnqueueTask(task);
            }

            return RedirectToAction("Index", new { siteId });
        }

        [Route("DiffTool/Compare/{refCodeId}/{compareToCodeId}")]
        public async Task<IActionResult> Compare(int refCodeId, int compareToCodeId)
        {
            GetClientCodeByIdQuery refCodeQuery = new GetClientCodeByIdQuery(refCodeId);
            var refCode = await Mediator.Send(refCodeQuery);

            GetClientCodeByIdQuery compareToCodeQuery = new GetClientCodeByIdQuery(compareToCodeId);
            ClientCodeDto compareToCode = await Mediator.Send(compareToCodeQuery);

            DiffViewViewModel model = new DiffViewViewModel();

            GetSiteLinkItemQuery getSiteLinkItemQuery = new GetSiteLinkItemQuery(refCode.SiteLinkItemId);
            var refSiteLinkItem = await Mediator.Send(getSiteLinkItemQuery);

            GetDiffIgnoreRegexQuery getDiffIgnoreRegexQuery = new GetDiffIgnoreRegexQuery(refSiteLinkItem.SiteItemId);
            DiffIgnoreRegexDto diffIgnoreRegexDto = await Mediator.Send(getDiffIgnoreRegexQuery);

            refCode.ApplyRegexDiffIgnore(diffIgnoreRegexDto);
            compareToCode.ApplyRegexDiffIgnore(diffIgnoreRegexDto);

            model.MapFrom(refCode, compareToCode);

            return View(model);
        }
    }
}
