﻿using Application.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Application.Common.Models;
using Application.DTOs;
using Application.Common.Interfaces;
using Application.Items.SiteItems.Commands.EditSiteItem;
using Application.Items.SiteItems.Queries;
using Application.Items.SiteLinkItems.Queries;
using WebUI.ViewModels.Sites;
using WebUI.Extensions;
using Application.Items.SiteItems.Commands.DeleteSiteItem;
using Application.Items.SiteLinkItems.Commands.DeleteSiteLinkItem;
using Application.Items.SiteLinkItems.Commands.EditSiteLinkItem;
using System.ComponentModel.DataAnnotations;

namespace WebUI.Controllers
{
    [Authorize]
    public class SitesController : ControllerBase
    {
        private readonly ILogger<SitesController> _logger;
        private readonly IMapper _mapper;
        private readonly Application.Common.Interfaces.IAuthorizerService authorizerService;

        public SitesController(ILogger<SitesController> logger, IMapper mapper, IAuthorizerService authorizerService)
        {
            _logger = logger;
            _mapper = mapper;
            this.authorizerService = authorizerService;
        }
        public async Task<IActionResult> IndexAsync([FromQuery] GetSiteItemsWithPaginationQuery query)
        {
            // Paging currently is not implemented on the front end, so we do set a default value to a large number.
            query.PageSize = 1000;
            PaginatedList<SiteItemDto> data = await Mediator.Send(query);
            ListViewModel model = new ListViewModel();
            model.MapFrom(data);
            return View("List", model);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            SiteItemDto existingSiteItem = null;
            EditSiteItemCommand editSiteItemCommand = null;

            if (id.HasValue)
            {
                existingSiteItem = await Mediator.Send(new GetSiteItemQuery(id.Value));

                if (existingSiteItem == null)
                    return NotFound();

                editSiteItemCommand = new EditSiteItemCommand();
                editSiteItemCommand.Id = existingSiteItem.Id;
            }

            if (!await this.authorizerService.Authorize<EditSiteItemCommand>(editSiteItemCommand))
                return NotFound();

            SiteEditViewModel siteEditViewModel = new SiteEditViewModel();
            siteEditViewModel.MapFrom(existingSiteItem);

            return View(siteEditViewModel); // TODO: Try inheriting to some ViewModel and check if front end validations still apply?
        }

        [HttpPost]
        public async Task<IActionResult> Edit(SiteEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                EditSiteItemCommand command = model.ToEditCommand();
                await Mediator.Send(command);
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public async Task<IActionResult> Panel(int id)
        {
            GetSiteItemQuery getSiteItemQuery = new GetSiteItemQuery(id);
            if (!await this.authorizerService.Authorize<GetSiteItemQuery>(getSiteItemQuery))
                return NotFound();

            SiteItemDto existingSiteItem = await Mediator.Send(getSiteItemQuery);
            GetSiteLinkItemsWithPaginationQuery getSiteLinkItems = new GetSiteLinkItemsWithPaginationQuery(existingSiteItem.Id);
            PaginatedList<SiteLinkItemDto> existingSiteLinkItem = await Mediator.Send(getSiteLinkItems);

            SiteItemPanelViewModel detailsViewModel = new SiteItemPanelViewModel();
            detailsViewModel.MapFrom(existingSiteItem, existingSiteLinkItem);

            return View(detailsViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> EditLink([FromForm] SiteLinkItemDetailsViewModel model)
        {
            EditSiteLinkItemCommand command = model.ToEditCommand();
            if (ModelState.IsValid)
            {
                await Mediator.Send(command);
                return RedirectToAction("Panel", new { id = model.SiteId });
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPost]
        public async Task<IActionResult> DeleteSiteLink([FromForm] int linkId, [FromForm] int siteId)
        {
            DeleteSiteLinkItemCommand command = new DeleteSiteLinkItemCommand(linkId);
            if (ModelState.IsValid)
            {
                var sucess = await Mediator.Send(command);
                if (sucess)
                {
                    return RedirectToAction("Panel", new { id = siteId });
                }
                else
                {
                    return BadRequest();
                }
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPost]
        public async Task<IActionResult> EditSitePartial(SiteEditViewModel model, [FromForm] string fieldName)
        {
            SiteItemDto existingSiteItem;
            existingSiteItem = await Mediator.Send(new GetSiteItemQuery(model.Id));

            if (existingSiteItem == null)
                return BadRequest();

            SiteEditViewModel siteEditViewModel = new SiteEditViewModel()
            {
                Host = existingSiteItem.Host,
                Id = existingSiteItem.Id,
                Title = existingSiteItem.Title
            };

            var value = model.GetType().GetProperty(fieldName).GetValue(model, null);
            siteEditViewModel.GetType().GetProperty(fieldName).SetValue(siteEditViewModel, value, null);

            EditSiteItemCommand command = siteEditViewModel.ToEditCommand();
            await Mediator.Send(command);
            return RedirectToAction("Panel", new { id = siteEditViewModel.Id });
        }
    }
}