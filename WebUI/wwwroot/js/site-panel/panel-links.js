﻿class LinksEditor {
    linksContainerId = 0;
    siteId = 0;
    links = [];
    editorFormTemplate = document.createElement('div');

    constructor(linksContainerId, siteId, links, editorFormId) {

        // Init properties
        this.editorFormTemplate = document.getElementById(editorFormId);
        let self = this;
        this.editorFormTemplate.childNodes[9].addEventListener("click", function () { self.closeEditor() });

        // Attach event to direct submit action of the Form to processForm() method
        if (this.editorFormTemplate.attachEvent) {
            this.editorFormTemplate.attachEvent("submit", function (e) { self.processForm(e) });
        } else {
            this.editorFormTemplate.addEventListener("submit", function (e) { self.processForm(e) });
        }

        this.linksContainerId = linksContainerId;
        this.siteId = siteId;
        this.links = links;

        // Init links
        this.links.forEach(link => {
            link.template = this.buildLinkContainerElement(link.url, link.linkId);
            link.isEditorOpen = false;
        });

        this.log('first link: ', this.links[0]);

        this.draw();
    }

    buildLinkContainerElement(url, linkId) {
        let template = `<div class="d-flex"><div class="mr-3">${url}</div><div class="btn btn-sm btn-light">edit</div><div class="btn btn-sm btn-light">delete</div></div>`;
        let elem = this.htmlToElement(template);
        let self = this;
        elem.childNodes[1].addEventListener("click", function (e) { self.startEditing(linkId) }, false);
        elem.childNodes[2].addEventListener("click", function (e) { self.deleteLink(linkId) }, false);
        return elem;
    }

    async deleteLink(linkId) {
        let form = new FormData();
        form.set("id", linkId);

        const response = await fetch(this.editorFormTemplate.action, {
            method: 'DELETE',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: form
        });

        var respStatusId = await response.status;

        if (respStatusId == 200) {
            console.log('deleted');
            this.links = this.links.filter(lnk => lnk.linkId != linkId);
            this.draw();
        } else {
            console.log('failed to delete');
        }
    }

    buildEditFormElement(linkId) {
        let link = this.findLinkByLinkId(linkId);
        this.editorFormTemplate[0].value = linkId;
        this.editorFormTemplate[2].value = link.url;

        return this.editorFormTemplate;
    }

    buildAddNewButton() {
        let template = `<div class="btn btn-primary">Add New</div>`;
        let elem = this.htmlToElement(template);
        let self = this;
        elem.addEventListener("click", function (e) { self.initNew() }, false);
        return elem;
    }

    initNew() {
        this.log('Init new item in edit mode.');
        this.closeEditor();
        let newElem = {
            linkId: 0,
            siteId: this.siteId,
            isEditorOpen: true,
            url: "some new url here"
        };
        this.links.push(newElem);

        newElem.template = this.buildLinkContainerElement(newElem.url, newElem.linkId);
        this.startEditing(0);
    }

    findLinkByLinkId(linkId) {
        let i = this.links.findIndex(lnk => lnk.linkId == linkId);
        return this.links[i];
    }

    startEditing(linkId) {
        this.log('Editing started: ', linkId);
        let link = this.findLinkByLinkId(linkId);

        if (!link.isEditorOpen) {
            this.closeEditor();
        }

        //let self = this;
        //elem.childNodes[1].childNodes[1].childNodes[9].childNodes[1].addEventListener("click", function (e) { self.closeEditor() }, false);

        link.template = this.buildEditFormElement(linkId);
        link.isEditorOpen = true;
        this.draw();

        $("#site-link-editor-form").validate({
            debug: true
        });
    }

    closeEditor() {
        console.log('Closing editor');
        let i = this.links.findIndex(lnk => lnk.isEditorOpen == true);
        if (i < 0)
            return;

        let link = this.links[i];
        link.template = this.buildLinkContainerElement(link.url, link.linkId);
        link.isEditorOpen = false;

        // Remove new link editor entry that was not saved
        if (link.linkId == 0) {
            this.links = this.links.filter(lnk => lnk.linkId != 0);
        }

        this.draw();
    }

    draw() {
        let container = document.getElementById(this.linksContainerId);
        container.innerHTML = '';
        this.links.forEach(link => {
            container.appendChild(link.template);
        });

        container.appendChild(this.buildAddNewButton());
    }

    async processForm(e) {
        console.log('processForm', e);
        let targetUrl = e.target.action;
        console.log(targetUrl);
        e.preventDefault();

        let isValidForm = $("#site-link-editor-form").valid();
        if (!isValidForm)
            return;

        console.log('Submitting form: ');
        await this.postData(e.target);
        /* do what you want with the form */

        // You must return false to prevent the default form behavior
        return false;
    }

    async postData(editorForm) {
        const form = new FormData(editorForm);

        const response = await fetch(editorForm.action, {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: form
        });

        var respStatusId = await response.status; 

        if (respStatusId == 200) {
            this.isSaved(await response.json());
        } else {
            this.notSaved();
        }
    }

    isSaved(e) {
        let link = this.findLinkByLinkId(e.linkId);
        if (link == undefined) {
            link = this.findLinkByLinkId(0);
        }

        link.url = e.url;
        link.linkId = e.linkId;
        link.siteId = e.siteId;
        this.closeEditor();
        this.draw();
    }

    notSaved() {
        this.closeEditor();
        this.draw();
    }

    // Helpers
    htmlToElement(html) {
        var template = document.createElement('template');
        html = html.trim(); // Never return a text node of whitespace as the result
        template.innerHTML = html;
        return template.content.firstChild;
    }

    log(msg, obj) {
        console.log(msg, obj);
    }
}