﻿let editorForm = document.getElementById("link-editor-form");
let currentlyHidden;
let currentLinkUrlContainer;
let currentWrapper;

function initEdit(linkInfoWrapper, linkUrlContainer, siteId, linkId) {
    cancelEdit();

    currentWrapper = document.getElementById(linkInfoWrapper);

    console.log('initEdit', linkInfoWrapper, linkUrlContainer, siteId, linkId);
    let wrapper = document.getElementById(linkInfoWrapper);

    currentLinkUrlContainer = document.getElementById(linkUrlContainer);

    initFormWithValues(siteId, linkId, document.getElementById(linkUrlContainer).innerText);
    replaceChildrenWithForm(wrapper, editorForm);
}

function cancelEdit() {
    console.log('cancelEdit');

    //document.getElementById('lc0')?.remove();
    //editorForm.parentElement.insertAdjacentElement('beforeBegin', editorForm);

    if (currentlyHidden != null) {
        for (let elem of currentlyHidden) {
            elem.style.display = "block";
        }
    }

    editorForm.style.display = "none";
}

function initFormWithValues(siteId, linkId, url) {
    console.log('initFormWithValues', siteId, linkId, url);
    document.getElementById("SiteId").value = siteId;
    document.getElementById("LinkId").value = linkId;
    document.getElementById("Url").value = url;

    $('.field-validation-error').addClass('field-validation-valid');
    $('.field-validation-error').removeClass('field-validation-error');
}

function replaceChildrenWithForm(wrapper, form) {
    console.log('replaceChildrenWithForm', wrapper, form);
    // hide all elements within wrapper
    elems = wrapper.children;
    for (let elem of elems) {
        elem.style.display = "none";
    }
    // put form within wrapper
    //TODO: this
    wrapper.appendChild(form);

    // display form
    form.style.display = "block";

    currentlyHidden = elems;
}

async function processForm(e) {
    console.log('processForm', e);
    let targetUrl = e.target.action;
    console.log(targetUrl);
    e.preventDefault();

    console.log('Submitting form: ');
    postData(e.target);
    /* do what you want with the form */

    // You must return false to prevent the default form behavior
    return false;
}

function isSaved(e) {
    console.log(e);
    cancelEdit();
    currentLinkUrlContainer.innerText = e.url;
    currentWrapper.replaceWith(htmlToElement(getNewContainer(e.siteId, e.linkId, e.url)));
}

function notSaved() {
    cancelEdit();
}

if (editorForm.attachEvent) {
    editorForm.attachEvent("submit", processForm);
} else {
    editorForm.addEventListener("submit", processForm);
}

async function postData(editorForm) {
    const form = new FormData(editorForm);

    const response = await fetch(editorForm.action, {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: form
    });

    var respStatusId = await response.status;

    if (respStatusId == 200) {
        isSaved(await response.json());
    } else {
        notSaved();
    }
}

function initNew(linksContainerId, siteId) {
    cancelEdit();
    let newContainer = getNewContainer(siteId, 0, '');
    let container = document.getElementById(linksContainerId);
    container.innerHTML += newContainer;
    initEdit(`lc${0}`, `sl${0}`, siteId, 0);
}

function htmlToElement(html) {
    var template = document.createElement('template');
    html = html.trim(); // Never return a text node of whitespace as the result
    template.innerHTML = html;
    return template.content.firstChild;
}

function getNewContainer(siteId, linkId, url) {
    let newContainer = `<div class="d-flex" id="lc${linkId}"><div class="mr-3" id="sl${linkId}">${url}</div><div class="btn btn-sm btn-light" onclick="initEdit('lc${linkId}', 'sl${linkId}', ${siteId}, ${linkId})">edit</div></div>`;
    return newContainer;
}
//function initEdit(linkInfoWrapper, linkUrlContainer, siteId, linkId)