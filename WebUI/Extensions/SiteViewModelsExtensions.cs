﻿using Application.Common.Models;
using Application.DTOs;
using Application.Items.SiteItems.Commands.EditSiteItem;
using Application.Items.SiteLinkItems.Commands.EditSiteLinkItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebUI.ViewModels.Sites;

namespace WebUI.Extensions
{
    public static class SiteViewModelsExtensions
    {
        internal static void MapFrom(this SiteEditViewModel model, SiteItemDto item)
        {
            model.Id = item?.Id ?? 0;
            model.Title = item?.Title;
            model.Host = item?.Host;
        }

        public static EditSiteItemCommand ToEditCommand(this SiteEditViewModel model)
        {
            return new EditSiteItemCommand()
            {
                Id = model?.Id ?? 0,
                Title = model?.Title,
                Host = model?.Host
            };
        }

        public static EditSiteLinkItemCommand ToEditCommand(this SiteLinkItemDetailsViewModel model)
        {
            return new EditSiteLinkItemCommand()
            {
                Id = model?.LinkId ?? 0,
                Url = model?.Url,
                SiteItemId = model?.SiteId ?? 0
            };
        }

        public static void MapFrom(this SiteItemDetailsViewModel model, SiteItemDto item)
        {
            model.Id = item?.Id ?? 0;
            model.Title = item?.Title;
            model.Host = item?.Host;
        }

        public static void MapFrom(this ListViewModel model, PaginatedList<SiteItemDto> sites)
        {
            model.CurrentPage = sites.PageIndex;
            model.Sites = sites.Items.Select(si => {
                var siteDetails = new SiteItemDetailsViewModel();
                siteDetails.MapFrom(si);
                return siteDetails;
            }).ToList();
            model.TotalPages = sites.TotalPages;
            model.TotalSites = sites.TotalCount;
        }

        public static void MapFrom(this SiteItemPanelViewModel model, SiteItemDto existingSiteItem, PaginatedList<SiteLinkItemDto> existingSiteLinkItem)
        {
            var details = new SiteItemDetailsViewModel();
            details.MapFrom(existingSiteItem);
            model.SiteItemDetails = details;

            SiteEditViewModel siteEditViewModel = new SiteEditViewModel();
            siteEditViewModel.MapFrom(existingSiteItem);
            model.SiteEditViewModel = siteEditViewModel;

            model.Links = existingSiteLinkItem.Items.Select(sli => {
                var linkDetails = new SiteLinkItemDetailsViewModel();
                linkDetails.MapFrom(sli);
                return linkDetails;
            }).ToList();
        }

        public static void MapFrom (this SiteLinkItemDetailsViewModel model, SiteLinkItemDto existingSiteLinkItem)
        {
            model.LinkId = existingSiteLinkItem.Id;
            model.Url = existingSiteLinkItem.Url;
            model.SiteId = existingSiteLinkItem.SiteItemId;
        }
    }
}
