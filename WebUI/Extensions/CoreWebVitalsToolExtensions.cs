﻿using Application.Common.Models;
using Application.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebUI.ViewModels.CoreWebVitalsTool;
using WebUI.ViewModels.DiffTool;
using WebUI.ViewModels.Sites;

namespace WebUI.Extensions
{
    public static class CoreWebVitalsToolExtensions
    {
        public static void MapFrom(this CoreWebVitalsToolMainViewModel model, SiteItemDto existingSiteItem, PaginatedList<SiteLinkItemDto> existingSiteLinkItem, CWVInfoDto latestCWVInfo)
        {
            var details = new SiteItemDetailsViewModel();
            details.MapFrom(existingSiteItem);
            model.SiteItemDetails = details;
            model.Links = existingSiteLinkItem.Items.Select(sli =>
            {
                var linkDetails = new LinkCWVInfoViewModel();
                linkDetails.MapFrom(sli, latestCWVInfo);
                return linkDetails;
            }).ToList();
        }

        public static void MapFrom(this LinkCWVInfoViewModel model, SiteLinkItemDto existingSiteLinkItem, CWVInfoDto cwvInfoDto)
        {
            model.LinkId = existingSiteLinkItem.Id;
            model.Url = existingSiteLinkItem.Url;
            model.SiteId = existingSiteLinkItem.SiteItemId;
            model.cwvEntryViewModel = cwvInfoDto!=null ? new CWVEntryViewModel()
                 {
                     Id = cwvInfoDto.Id,
                     SiteLinkItemId = existingSiteLinkItem.Id,
                     UrlUsed = cwvInfoDto.UrlUsed,
                     Performance = cwvInfoDto.Performance,
                     SEO = cwvInfoDto.SEO,
                     BestPractices = cwvInfoDto.BestPractices,
                     Accessibility = cwvInfoDto.Accessibility,
                     IsScanSuccessful = cwvInfoDto.IsScanSuccessful,
                     IsScanCompleted = cwvInfoDto.IsMetricScanCompleted,
                 } : null;
        }
    }
}
