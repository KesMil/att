﻿using Application.Common.Models;
using Application.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WebUI.ViewModels.DiffTool;
using WebUI.ViewModels.Sites;

namespace WebUI.Extensions
{
    public static class DiffToolViewModelsExtensions
    {
        public static void ApplyRegexDiffIgnore(this ClientCodeDto clientCodeDto, DiffIgnoreRegexDto diffIgnoreRegexDto)
        {
            if (diffIgnoreRegexDto == null || diffIgnoreRegexDto.RegexLines == null)
                return;

            foreach (var reg in diffIgnoreRegexDto.RegexLines.Split("\r\n"))
            {
                if (clientCodeDto.Code != null)
                    clientCodeDto.Code = Regex.Replace(clientCodeDto.Code, reg, "R_E_M_O_V_E_D");
            }
        }
        public static void ApplyRegexDiffIgnore(this List<ClientCodeDto> clientCodes, DiffIgnoreRegexDto diffIgnoreRegexDto)
        {
            if (diffIgnoreRegexDto == null)
                return;

            foreach (var cc in clientCodes)
            {
                cc.ApplyRegexDiffIgnore(diffIgnoreRegexDto);
            }
        }
        public static void MapFrom(this DiffToolMainViewModel model, SiteItemDto existingSiteItem, PaginatedList<SiteLinkItemDto> existingSiteLinkItem, List<ClientCodeDto> allClientCodes, List<ClientCodeDto> refClientCodes, DiffIgnoreRegexDto diffIgnoreRegexDto)
        {
            // Transform client source codes by ignore regexes
            allClientCodes.ApplyRegexDiffIgnore(diffIgnoreRegexDto);
            refClientCodes.ApplyRegexDiffIgnore(diffIgnoreRegexDto);

            // Remove reference (base) codes from all codes
            allClientCodes = allClientCodes.Where(cc => !refClientCodes.Exists(rcc => rcc.Id == cc.Id)).ToList();

            // Map
            var details = new SiteItemDetailsViewModel();
            details.MapFrom(existingSiteItem);
            model.SiteItemDetails = details;
            model.DiffIgnoreRegex = diffIgnoreRegexDto?.RegexLines;
            model.Links = existingSiteLinkItem.Items.Select(sli => {
                var linkDetails = new SiteLinkItemDiffDetailsViewModel();
                linkDetails.MapFrom(sli, allClientCodes.Where(rcc => rcc.SiteLinkItemId == sli.Id).ToList(), refClientCodes.Where(rcc => rcc.SiteLinkItemId == sli.Id).FirstOrDefault());
                return linkDetails;
            }).ToList();
        }

        public static void MapFrom(this SiteLinkItemDiffDetailsViewModel model, SiteLinkItemDto existingSiteLinkItem, List<ClientCodeDto> allClientCodes, ClientCodeDto refClientCode)
        {
            model.LinkId = existingSiteLinkItem.Id;
            model.Url = existingSiteLinkItem.Url;
            model.SiteId = existingSiteLinkItem.SiteItemId;
            model.AllClientCodes = allClientCodes.Select(cc => {
                var clientCode = new ClientCodeListEntryViewModel();
                clientCode.MapFrom(cc, refClientCode);
                return clientCode;
            }).ToList();
            if (refClientCode != null)
            {
                model.ReferenceClientCode = new ClientCodeListEntryViewModel();
                model.ReferenceClientCode.MapFrom(refClientCode, refClientCode);
            }
        }

        public static void MapFrom(this ClientCodeListEntryViewModel model, ClientCodeDto clientCode, ClientCodeDto refClientCode)
        {
            model.Id = clientCode.Id;
            model.IsDownloadingCompleted = clientCode.IsDownloadingCompleted;
            model.SiteLinkItemId = clientCode.SiteLinkItemId;
            model.UrlUsed = clientCode.UrlUsed;
            model.ResponseCode = clientCode.ResponseCode;
            model.IsDifferentFromReferenceCode = clientCode.Code != refClientCode?.Code;
        }

        public static void MapFrom(this DiffViewViewModel model, ClientCodeDto refCode, ClientCodeDto compareToCode)
        {
            model.ReferenceContent = refCode.Code;
            model.CompareToContent = compareToCode.Code;
        }
    }
}