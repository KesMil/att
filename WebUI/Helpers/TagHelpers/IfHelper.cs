﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebUI.Helpers.TagHelpers
{
    [HtmlTargetElement("*")]
    public class IfHelper : TagHelper
    {
        public bool? DisplayIf { get; set; }
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (DisplayIf.HasValue && !DisplayIf.Value)
                output.SuppressOutput();
        }
    }
}