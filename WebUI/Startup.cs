using Application.Interfaces;
using Hangfire;
using Hangfire.Mongo;
using Hangfire.Mongo.Migration.Strategies.Backup;
using Hangfire.Mongo.Migration.Strategies;
using Infrastructure.Identity;
using Infrastructure.Persistence;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebUI.Infrastructure;
using System.Reflection;
using MediatR;
using FluentValidation;
using System;
using Application;
using FluentValidation.AspNetCore;
using Application.Common.Mappings;
using Westwind.AspNetCore.LiveReload;
using Infrastructure;

namespace WebUI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLiveReload(config =>
            {
                // optional - use config instead
                //config.LiveReloadEnabled = true;
                //config.FolderToMonitor = Path.GetFullname(Path.Combine(Env.ContentRootPath,"..")) ;
            });

            services.AddHttpContextAccessor();

            services.AddTransient<IDependencyResolver, DependencyResolver>();
            services.AddTransient<ITasksManager, TasksManager>();

            services.AddApplication();

            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());

            services.AddHangfire(c =>
            {
                c.UseMongoStorage(Configuration.GetConnectionString("HangfireConnection"), new MongoStorageOptions() {
                    MigrationOptions = new MongoMigrationOptions()
                    {
                        MigrationStrategy = new MigrateMongoMigrationStrategy(),
                        BackupStrategy = new CollectionMongoBackupStrategy()
                    }
                });
            });

            services.AddDbContext<ApplicationDbContext>(options =>
                    options.UseSqlServer(
                        Configuration.GetConnectionString("DefaultConnection"),
                        b => b.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)));
            services.AddScoped<IApplicationDbContext>(provider => provider.GetService<ApplicationDbContext>());

            services.AddSingleton<IEmailSender, IdentityEmailSender>();
            services.AddSingleton<ICurrentUserService, CurrentUserService>();

            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
            {
                options.SignIn.RequireConfirmedAccount = false;
                options.Password.RequireDigit = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
            })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddMvc()
            .AddRazorPagesOptions(options =>
            {
                options.Conventions.AddAreaPageRoute("Identity", "/Account/Login", "/login");
                options.Conventions.AddAreaPageRoute("Identity", "/Account/ForgotPassword", "/forgot-password");
                options.Conventions.AddAreaPageRoute("Identity", "/Account/Register", "/register");
                options.Conventions.AddAreaPageRoute("Identity", "/Account/AccessDenied", "/access-denied");
                options.Conventions.AddAreaPageRoute("Identity", "/Account/ConfirmEmail", "/confirm-email");
                options.Conventions.AddAreaPageRoute("Identity", "/Account/ConfirmEmailChange", "/confirm-email-change");
                options.Conventions.AddAreaPageRoute("Identity", "/Account/ExternalLogin", "/external-login");
                options.Conventions.AddAreaPageRoute("Identity", "/Account/ForgotPasswordConfirmation", "/forgot-password-confirmation");
                options.Conventions.AddAreaPageRoute("Identity", "/Account/Lockout", "/lockout");
                options.Conventions.AddAreaPageRoute("Identity", "/Account/LoginWith2fa", "/Login-2f");
                options.Conventions.AddAreaPageRoute("Identity", "/Account/LoginWithRecoveryCode", "/login-rc");
                options.Conventions.AddAreaPageRoute("Identity", "/Account/Logout", "/logout");
                options.Conventions.AddAreaPageRoute("Identity", "/Account/RegisterConfirmation", "/register-confirmation");
                options.Conventions.AddAreaPageRoute("Identity", "/Account/ResendEmailConfirmation", "/resend-email-confirmation");
                options.Conventions.AddAreaPageRoute("Identity", "/Account/ResetPassword", "/reset-password");
                options.Conventions.AddAreaPageRoute("Identity", "/Account/ResetPasswordConfirmation", "/reset-password-confirmation");

                options.Conventions.AuthorizeAreaFolder("Identity", "/Account/Manage");
                options.Conventions.AuthorizeAreaPage("Identity", "/Account/Logout");
            }).AddRazorRuntimeCompilation().AddFluentValidation();

            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = $"/Identity/Account/Login";
                options.LogoutPath = $"/Identity/Account/Logout";
                options.AccessDeniedPath = $"/Identity/Account/AccessDenied";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseLiveReload();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseHangfireDashboard();

            app.UseRouting();

            // This must appear between .UseRouting() and UseEndpoints()
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
